// CRUD Operations (MongoDB)

// CRUD Operation are the heart of any backend app.
// Mastering CRUD Operations is essential for any developers

/*
C - Create/Insert
R - Retrieve/Read
U - Update
D - Delete
*/

// [SECTION] Creating Documents

/*

SYNTAX:

db.users.insertOne({object})



*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
});

db.users.insertOne({
	firstName: "TEST",
	lastName: "TEST",
	age: 0,
	contact: {
		phone: "000000",
		email: "TEST@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.insertOne({
	firstName: "Vermillon",
	lastName: "G",
	age: 18,
	contact: {
		phone: "09123456789",
		email: "xxvermillonxx90@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
});



/*

SYNTAX:

db.users.insertOne([{object}])


*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["PHP", "REACT", "PYTHON"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["REACT", "LARAVEL", "SASS"],
	department: "none"
}
]);

// [SECTION] -READ/RETRIEVE

/*

SYNTAX:

db.users.findOne();
db.users.findOne({field: value});


*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});

// Multiple Criteria

db.users.find({department: "none", age: 82});



// [SECTION] Updating a Data

/*

SYNTAX:
db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
{firstName: "TEST"},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "123456789",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "Active"
	}
}
);

// find the data
db.users.findOne({firstName: "Bill"});
db.users.findOne({firstName: "TEST"});

db.users.findOne({"contact.email": "billgates@gmail.com"});

// Update Many Collection

/*

SYNTAX:
db.users.updateMany({criteria, {$set: {field: value}}});

*/

db.users.updateMany(
{department: "none"},
{
	$set: {
		department: "HR"
	}
}
);

// [SECTION] Delete a Data

db.users.insert({
	firstName: "TEST"
});

// Delete single Document

/*

SYNTAX:
db.users.deleteOne({criteria});

*/

db.users.deleteOne({
	firstName: "TEST"
});

// Delete Many

/*

SYNTAX:
db.users.deleteMany({criteria});

*/


db.users.insert({
	firstName: "Bill"
});


db.users.deleteMany({firstName: "Bill"});