let http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {
  if (req.url == '/profile' && req.method == 'GET') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Welcome to your profile');
    res.end();
  } else if (req.url == '/courses' && req.method == 'GET') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write("Here's our courses available");
    res.end();
  } else if (req.url == '/addcourse' && req.method == 'POST') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Add a course to our resources');
    res.end();
  } else if (req.url == '/updatecourse' && req.method == 'PUT') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Update a course from our resources');
    res.end();
  } else if (req.url == '/archivecourse' && req.method == 'DELETE') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Archive a course from our resources');
    res.end();
  } else {
    req.url = '';
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Welcome to Booking System');
    res.end();
  }
});

server.listen(port, () => console.log(`Server is running at localhost:${port}`));