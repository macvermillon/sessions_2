// [SECTION] JS Synchronous vs Asynchronous
// By default JS is Synchronous -> only one statement at a time

// JS Code Reading
// Top to Bottom, Left to Right

console.log("Hellon World!"); //1

// for(let i = 0; i <= 1500; i++){ //3
// 	console.log(i);
// }

console.log("Hello Agian!") //2

// [SECTION] Getting all posts
// fetch("URL");

// fetch("URL").then((response) => {})

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

// JSON() will convert response object/promise into JSON format
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(json => console.log(json));

 // "async" and "await" keywords is another approach that can be used to achieve

async function fetchData(){

	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	console.log(result);
	
	console.log(typeof result);
	
	// We can acces the content of the response by directly accessing it's body property
	console.log(result.body);


	let json = result.json();
	console.log(json);
}

fetchData();

// [SECTION] Getting a specific posts

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Creating a post 
/*

SYNTAX:
fetch("URL", options)
.then(resonse -> {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again!",
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});