const Task = require("../models/Task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		// req.body.name === request
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
		}else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
		}else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, requestBody) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return res.send("Cannot find data with provided ID");
		}
		result.name = requestBody.name;
		
		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return res.send("there is an error while saving the update.")
			}else{
				return updatedTask;
			}
		})
	})
}


module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return res.send("Cannot find data with provided ID");
		}else{
			return result;
		}	
	});
};

module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return res.send("Cannot find data with provided ID");
		}
		result.status = "Completed";
		
		return result.save().then((completeTask, error) => {
			if(error){
				console.log(error);
				return res.send("there is an error while saving the update.")
			}else{
				return completeTask;
			}
		});
	});
};