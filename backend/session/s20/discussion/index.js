// console.log("Hello World!");

// [SECTION] Arithmetic Operators

let x = 26;
let y = 5;

let sum = x + y;
console.log("Result from addition operator: " + sum);

let difference = x - y;
console.log("Result from subtraction operator: " + difference);

let product = x * y;
console.log("Result from multiplication operator: " + product);

let quotient = x / y;
console.log("Result from division operator: " + quotient);

//  modulo %

let remainder = x % y;
console.log("Result from modulo operator: " + remainder);

// [SECTION] Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("addition assigment: " + assignmentNumber);

// Shorthand Method for Assignment Method

assignmentNumber += 2;
console.log("addition assigment: " + assignmentNumber);

assignmentNumber -= 2;
console.log("subtraction assigment: " + assignmentNumber);

assignmentNumber *= 2;
console.log("multiplication assigment: " + assignmentNumber);

assignmentNumber /= 2;
console.log("division assigment: " + assignmentNumber);

// Multiple Operators and Parentheses

/*
            - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
        */

let mdas = 1+2-3*4/5;
console.log("result from mdas operation: " + mdas);

/*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
        */

let pemdas = 1 + (2 -3) * (4 / 5);
console.log("Result from pemdas operation: " + pemdas);

// Increment and Decrement
// Incrementation -> ++
// Decrementaion -> --

let z = 1;

// Pre-Increment
let increment = ++z;
console.log("Pre-increment: " + increment);
console.log("Pre-increment: " + z);

// Post-Increment
increment = z++;
console.log("Post-increment: " + increment);

let myNum1 = 1;
let myNum2 = 1;

let preIncrement = ++myNum1;
preIncrement = ++myNum1;
console.log(preIncrement);

let postIncrement = myNum2++;
postIncrement = myNum2++;
console.log(postIncrement);

let myNumA = 5;
let myNumB = 5;

let preDecrement = --myNumA;
preDecrement = --myNumA;
console.log(preDecrement);

let postDecrement = myNumB--;
postDecrement = myNumB--;
console.log(postDecrement);

// [SECTION] Type Coertion
// Automatic or Implicit conversion of value form one data tpye to another

let myNum3 = "10";
let myNum4 = 12;

let coercion = myNum3 + myNum4;
console.log(coercion);
console.log(typeof coercion);

let myNum5 = true + 1;
console.log(myNum5);

let myNum6 = false + 1;
console.log(myNum6);

// [SECTION] Comparion Operator

let juan = "juan";

// Equality Operator (==)
console.log(juan == "juan");
console.log(1 == 2);
console.log(false == 0);

// Inequality Operator (!=)
// ! -> NOT

console.log(juan != "juan"); 
console.log(1 != 2);
console.log(false != 0);

// Strict Equality Operator (===)
// Same data type, content and value
console.log(1 === 1);
console.log(false === 0);

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(false !== 0);

// [SECTION] Relational Operator
// Comparsion Operator whether one value greater or less than to othe value

let a = 50;
let b = 65;

let exapmle1 = a > b;
console.log(exapmle1);

let exapmle2 = a < b;
console.log(exapmle2);

let exapmle3 = a >= b;
console.log(exapmle3);

let exapmle4 = a <= b;
console.log(exapmle4);

// [SECTION] Logical Operator

// Logical "AND" (&& - Double Ampersands )

let isLegalAge = true;
let isRegistered = true;

// All conditions should be true
let allRequirementsMet = isLegalAge && isRegistered;

console.log("Logical AND: " + allRequirementsMet);

// Logical "OR" (|| - Double Pipe)

let isLegalAge2 = true;
let isRegistered2 = false;

let someRequirementsMent = isLegalAge2 || isRegistered2;
console.log(someRequirementsMent);

// Logical "NOT" (! -)
let isRegistered3 = false;

let someRequirementsNotMet = !isRegistered3
console.log("Result from logical NOT: " + someRequirementsNotMet);








