// Node.js
// Client - Server Architecture

// Client Applications -> Browsers
// DB Servers -> System Developed using Node
// DB -> MongoDB

// Benefits
// 1. Performance -> optimized for Web Applications
// 2. Familarity -> Same Old JS
// NPM - Node Package Manager

// Use the "require" directive to load Node.js
// "http module" contains the components to create a server

let http = require("http");

// "createserver()" listens to a request from the client
// A port is a Virtual pint where network connecton start and end
// ".listen()" where the server will listen to any request sent in our server
http.createServer(function(request, response){

	// "writeHead()" sets the status code for response
	// status code 200 means ok
	response.writeHead(200, {"Content-Type" : "text/plain"});

	// Send the response with text content "Hello World!"
	response.end("Hello World!")

}).listen(4000);

console.log("Server is running at localhost:4000");