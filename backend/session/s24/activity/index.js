// console.log("Hello World");

//Objective 1
//Add code here
//Note: function name is numberLooper
let loopCount = prompt('Enter a number');
console.log(`The number you provided is ${loopCount}`);

function numberLooper(num1) {
  let num2 = [];
  for (let i = num1; i >= 0; i--) {
    num2[i] = i;
    if (num2[i] % 10 === 0 && num2[i] > 50) {
      console.log('The number is divisible by 10. Skipping the number.');
    } else if (num2[i] % 5 === 0 && num2[i] > 50) {
      console.log(num2[i]);
    }
    if (num2[i] == 50) {
      console.log('The current value is at 50. Terminating the loop.');
      break;
    }
  }
}
numberLooper(loopCount);

//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for (let i = 0; i < string.length; i++) {
  if (string[i] !== 'a' && string[i] !== 'e' && string[i] !== 'i' && string[i] !== 'o' && string[i] !== 'u') {
    filteredString += string[i];
  }
}
console.log(filteredString);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
    numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null,
  };
} catch (err) {}
