// Parameters and Arguements

function printName(name){
	console.log("My name is " + name);
}

// Calling or Invoking our Function "printName"
// Arguement --> Invoke
printName("Juana");
printName("Vermillon");

let sampleVariabe = "John";

printName(sampleVariabe);

// Check Divisibility

function checkDivisibilty(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

// Invocation
checkDivisibilty(64);

// Functions as Arguments

function argumentFunction(){
	console.log("This function was passed as an argument.")
}


function invokeFunction(argument){
	argumentFunction();
}
invokeFunction();

// Multiple Parameters in a Function

function createFullName(firstName, middleName, lastname){
	console.log(firstName + " " + middleName + " " + lastname);
}

// Multiple Argument
createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela")
createFullName("Juan", "Dela", "Cruz", "Hello");

// Function with Alert Message
function showSampleAlert(){
	alert("Hello User");
}

// showSampleAlert();

console.log("Testing");

// Prompt

// let samplePrompt = prompt("Enter Your Name");
// console.log("Hello, " + samplePrompt);



// Function with Prompts

function printWelcomeMessage(){
	let firstName = prompt("Enter your First Name");
	let lastName = prompt("Enter your Last Name");

	console.log("Hello " + firstName + " " + lastName + "!");
	console.log("Welcome to my Page")
}

printWelcomeMessage();

function greeting(){
	return "Hello, this is the return statement"
	// code below return statement will never work
	// return -> breaking statement of the function.
	console.log("Hello this is Me")
}
let greetingFunction = greeting();
console.log(greetingFunction);

console.log(greeting());