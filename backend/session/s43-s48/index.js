// [SECTION] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Emvironment Variables
require("dotenv").config();

// Access Routes
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course.js")
// Allows access to routes defined within our application

// [SECTION] Environment Setup
const port = 4000;

// [SECTION] Server Setup
const app = express();

// Routes
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// [SECTION] Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.0fcazrx.mongodb.net/S43-S48?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

// DB Connection Check
const dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'connection error:'));
dbConnection.once('open', () => {
  console.log('Connected to Atlas Cloud Database');
});

// [SECTION] Backend Routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// [SECTION] Server Gateway Response
if(require.main === module){
    app.listen(process.env.PORT || port, () => {
        console.log(`API is now online on port ${process.env.PORT || port}`);
    });
};

module.exports = app;