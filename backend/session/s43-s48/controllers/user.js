// [SECTION] Dependencies and Modules
const User = require("../models/Users");
const bcrypt = require("bcrypt");

const auth = require("../auth");
const Course = require("../models/Course");

// check email exist

// STEPS: 
// Use mongoose "find" method to find duplicate emails.
// Use the "then" method to send a response back to the frontend application based on the result of the "find" method.
module.exports.checkEmailExist = (reqBody) => {
    console.log(reqBody);
    console.log(reqBody.email);
    return User.find({email: reqBody.email}).then(result => {
        console.log(result);
        if(result.length > 0) {
            return true
        }else {
            return false
        };
    });
};

// User registration
// STEPS:
// Create a new User object using the mongoose model and the information from the request

module.exports.registerUser = (reqBody) => {
    const newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    });
    return newUser.save().then((user, error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
    .catch(err => err);
}

// User Authentication
module.exports.loginUsers = (req, res) => {
    return User.findOne({email: req.body.email}).then(result => {
        console.log(result);
        if(result == null){
            return res.send(false); 
        }else{
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            console.log(isPasswordCorrect);
            if(isPasswordCorrect){
                return res.send({access: auth.createAccessToken(result)})
            }else{
                return res.send(false);
            }
        }
    })
    .catch(err => res.send(err))
}

// Get User Details by ID
module.exports.getProfile = (userId) => {
    return User.findById(userId).then((result) => {
      const profile = {
        _id: result._id,
        firstName: result.firstName,
        lastName: result.lastName,
        email: result.email,
        password: '',
        isAdmin: result.isAdmin,
        mobileNo: result.mobileNo,
        enrollments: result.enrollments,
      };
      return profile;
    });
};

// Retrieve user details

module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}

// Enroll users
// module.exports.enroll = async (req, res) => {
//     console.log(req.user.id);
//     console.log(req.body.courseId);

//     if(req.user.isAdmin){
//         return res.send("You cannot do this action")
//     }

//     let isUserUpdated = await User.findById(req.user.id).then(user => {
//         let newEnrollment = {
//             courseId: req.body.courseId,
//             courseName: req.body.courseName,
//             courseDescription: req.body.courseDescription,
//             coursePrice: req.body.coursePrice 
//         }

//         user.enrollments.push(newEnrollment);

//         return user.save().then(user => true).catch(error => res.send(error));
//     })

//     if(isUserUpdated !== true){
//         return res.send({message: isUserUpdated})
//     }

//     let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
//         let enrollee ={
//             userId: req.user.id
//         }

//         course.enrollees.push(enrollee);

//         return course.save().then(course => true).catch(error => res.send( error));
//     })

//     if(isCourseUpdated !== true){
//         return res.send({message: isCourseUpdated});
//     }

//     if(isUserUpdated && isCourseUpdated){
//         return res.send("You are now enrolled to a course. Thank you!");
//     }else{
//         return res.send("Something went wrong. Please try again!");
//     }
// };

module.exports.enroll = (req, res) => {
  if(req.user.isAdmin) {
    return res.send("You cannot do this action.");
  }
  let isUserEnrolled = Course.findById(req.body.courseId).then((course) => {
    let newEnrollment = {
      courseId: course._id,
      courseName: course.name,
      courseDescription: course.description,
      coursePrice: course.price,
    };
    let newEnrollee = {
      userId: req.user.id,
    };
    course.enrollees.push(newEnrollee);
    let enrollNow = course
      .save()
      .then((course) => true)
      .catch((error) => res.send(error));
    if(enrollNow) {
      User.findById(req.user.id).then((user) => {
        user.enrollments.push(newEnrollment);

        return user
          .save()
          .then((user) => true)
          .catch((error) => res.send(error));
      });
    }
  });
  if(isUserEnrolled) {
    return res.send("Successfully Enrolled");
  }else {
    return res.send({ message: isUserEnrolled });
  }
};

// Get Enrollments

// Activate User
module.exports.activateUser = (req, res) => {
    let activateUser = {
      isActive: true,
    };
  
    User.findById(req.params.userId).then((result, err) => {
      if (!result.isActive) {
        return Users.findByIdAndUpdate(req.params.userId, activateUser).then((active, err) => {
          if (err) {
            return res.send(err);
          } else {
            return res.send(`User: ${active.firstName} ${active.lastName} is now active.`);
          }
        });
      } else {
        return res.send("This user is already active");
      }
    });
  };
  
// Deactivate User
module.exports.deactivateUser = (req, res) => {
    let deactivateUser = {
      isActive: false,
    };
  
    User.findById(req.params.userId).then((result, err) => {
      if (result.isActive) {
        return Users.findByIdAndUpdate(req.params.userId, deactivateUser).then((deactive, err) => {
          if(error) {
            return res.send(error);
          } else {
            return res.send(`User: ${deactive.firstName} ${deactive.lastName} is now deactivated.`);
          }
        });
      } else {
        return res.send('This user is already deactivated');
      }
    });
  };
  
module.exports.getEnrollments = (req, res) => {
  User.findById(req.user.id).then((result, error) => {
    if(error) {
      return res.send(error);
    }else {
      return res.send(result.enrollments);
    }
  });
};

// Activity 46
// module.exports.getEnrollments = (req, res) => {
//   User.findById(req.user.id).then(result => {
//     if(result.enrollments === [] || result.enrollments == null){
//       return res.send("you are not enrolled to any courses.")
//     }else{
//       res.send(result.enrollments);
//     }
//   }).catch(error => res.send(error));
// }

module.exports.resetPassword = async (req, res) => {
  try{
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await User.findByIdAndUpdate(userId, {password: hashedPassword});

    res.status(200).json({message: "Password reset successfully"});
  }catch(error){
    res.status(500).json({message: "Internal Server Error"});
  }
}