// [SECTION] Dependencies and Modules
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, secret, {});
};

// Retrieve token from the request header
module.exports.verify = (req, res, next) => {
    // Token is retrieved from the request header
    // Authorizaton (Auth Tab) > Bearer Token

    console.log(req.headers.authorization);

    let token = req.headers.authorization;

    if(typeof token === "undefined"){
        return res.send({auth: "Failed, No Token!"});
    }else{
        console.log(token);

        token = token.slice(7, token.length);

        console.log(token);

        jwt.verify(token, secret, function(error, decodedToken){
            if(error){
                return res.send({
                    auth: "Failed",
                    message: error.message
                });
            }else{
                console.log(decodedToken);
                // Contains data from our token

                req.user = decodedToken;

                next();
                // Will let us proceed to the next controller
            }
        });
    }
};

// Verify User Level
module.exports.verifyAdmin =(req, res, next) => {
    if(req.user.isAdmin) {
        next();
    }else{
        return res.send({
            auth: "Failed!",
            message: "Access Forbidden",
        });
    }
};