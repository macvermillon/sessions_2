// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user")
const auth = require("../auth.js");

// Destruture from Auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

// check email routes
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// U
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", userController.loginUsers);

router.post('/details/:id', (req, res) => {
    userController.getProfile(req.params.id).then((resultFromController) => res.send(resultFromController));
});

// retrieve user details
router.post("/details", verify, (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/details", verify, userController.getProfile);

// Enroll user to a course
router.post("/enroll", verify, userController.enroll);

// Get Enrollments
router.get("/getEnrollments", verify, userController.getEnrollments);

router.put("/reset-password", verify, userController.resetPassword);

// [SECTION] Export Route System
module.exports = router;

