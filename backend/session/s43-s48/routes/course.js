const express = require("express");
const courseController = require("../controllers/course.js");

const auth = require("../auth.js");

// Destructing of verify and verifyAdmin
const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create a course POST Method
router.post("/", verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get("/all", courseController.getAllCourses);

// Get all "active" course
router.get("/", courseController.getAllActive);

// Get specific course using its id 
router.get("/:courseId", courseController.getCourse);

// Updating a specific course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// // Archiving a specific active course (Admin Only)
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// // Activating a specific inactive Course (Admin Only)
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

router.post("/archives", verify, verifyAdmin, courseController.archives);


module.exports = router;