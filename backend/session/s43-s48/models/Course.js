// [SECTION] Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const courseSchema = new mongoose.Schema({
    name : {
        type: String,
        required: [true, "Course is required!"]
    },
    description : {
        type: String,
        required: [true, "Description is required!"]
    },
    price : {
        type: Number,
        required: [true, "Price is required!"]
    },
    isActive : {
        type: Boolean,
        default: true
    },
    createdOn : {
        type: Date,
        // The "new Date()" expression that initiates a new "date" that store the current time and data whenever a course is created in our database
        default: new Date()
    },
    enrollees : [
        {
            userId: {
                type: String,
                required: [true, "UserId is required!"]
            },
            enrolledOn: {
                type: Date,
                dafault: new Date()
            } 
        }
    ]
});

module.exports = mongoose.model("Course", courseSchema);