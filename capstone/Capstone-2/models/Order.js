// Mongoose Dependency
const mongoose = require('mongoose');

// Schema/Blueprint
const orderSchema = new mongoose.Schema({

    userId: {
      type: String,
      ref: 'User',
      required: [true, 'User Id is required']
    },
    products: [{
    productId: {
      type: String,
      ref: 'Product',
      required: [true, 'Product Id is required']
    },
    quantity: {
      type: Number,
      required: [true, 'Quantity is required']
    },
    price: {
      type: Number,
      ref: 'Product',
      required: [true, 'Product price is required']
    }
    }],
    totalAmount: {
      type: Number,
      required: [true, 'Total amount is required']
    },
    purchasedOn: {
      type: Date,
      default: Date.now
    }
});

// Model
module.exports = mongoose.model('Order', orderSchema);