// Mongoose Dependency
const mongoose = require('mongoose');


// Schema/Blueprint
const productSchema = new mongoose.Schema({

  name: {
    type: String,
    required: [true, 'Product Name is required']
  },
  description: {
    type: String,
    required: [true, 'Description is required']
  },
  price: {
    type: Number,
    required: [true, 'Product Price is required']
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
  default: new Date()
  }
});

// Model
module.exports = mongoose.model('Product', productSchema);