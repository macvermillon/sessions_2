// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

// destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();

// Route to purchase a product.
router.post("/purchase", verify, orderController.purchaseProduct);

// Route to get all orders of a user (Admin)
router.get('/allOrders', verify, verifyAdmin, orderController.getAllOrders);

// Route to get all orders of a user (User)
router.get('/userOrders', verify, orderController.getUserOrders);



















// Allows us to export the "router" object
module.exports = router; 