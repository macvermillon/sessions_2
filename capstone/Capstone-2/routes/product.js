// Dependencies and Modules
const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');

// destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();

// [SECTION] Create product route accessible by admin only
router.post("/create", verify, verifyAdmin, productController.createProduct);

// [SECTION] Route for retrieving all the products (Admin)
router.get("/all", productController.getAllProducts);

// [SECTION] Route for retrieving all the ACTIVE products for all the users.
router.get("/active", productController.getAllActive);

// [SECTION] Route for retrieving single product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and may change depending on the information provided in the url.
router.get("/:productId", productController.getProduct);

// [SECTION] Route for updating a Product (Admin)
// add JWT authentication for verifying if the user is and admin or not. Admin will only have the access to update the product.
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// [SECTION] Route for archive a product (Admin)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// [SECTION] Activating a Product (Admin)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// Route to search for product by product name
router.post('/search', productController.searchProductsByName);

// Route for products based on price range
router.post('/search-price', productController.searchProductByPriceRange);

















// Allows us to export the "router" object
module.exports = router;