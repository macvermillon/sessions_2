// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

// destructure the auth file:
const { verify, verifyAdmin } = auth;

const router = express.Router();

// Route for checking if user email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for authentication
// Here we have streamlined the login routes by directly invoking the "loginUser" function. Consiquently, the req.body will now be incorporated into the controller function
router.post("/login", userController.loginUser);

// Route for retrieving user details
router.get("/details", verify, userController.getProfile);

// Route for updating a user as an admin
router.put('/update-user-admin', verify, verifyAdmin, userController.updateUserAsAdmin);

// Route for resetting the password
router.put('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);

//Route to activating a course (Admin)
router.put("/:userId/activate", verify, verifyAdmin, userController.activateUser);


























module.exports = router;