	import Banner from '../components/Banner.js';
	import Footer from '../components/Footer.js';
	import { Container } from 'react-bootstrap';
	import Highlights from '../components/Highlights.js';

	export default function Home(){		
		const data = {
			title: "POWER MAC CENTER",
			content: "Your Premier Apple Partner",
			destination: "/",
			label: "Shop Now!"
		}
		return (
			<Container classname='container-fluid' id="appHome">
				<>
					<Banner passed_data= {data}/>
					<Highlights/>

				</>
				<Footer>
				
					 <p>&copy; {new Date().getFullYear()} PowerMacCenter</p>
	
				</Footer> 
			</Container>
		)
	}


