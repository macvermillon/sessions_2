import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col, img} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from "../UserContext.js";
import {Link, NavLink} from 'react-router-dom';

export default function CourseView(){
    /*Curly brace must be used on useParams and useContext*/
    const {user, setUser} = useContext(UserContext);
    const { prodID } = useParams();

    const navigate = useNavigate();

	const [name, setName] = useState("");
    const [course, setCourse] = useState("")
	const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [quantity, setQuantity] = useState(1);





    const addToCart = (productId_parameter) => {
        if(quantity < 1){
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong...',
              text: `Quantity must be greater than 0.`,
            })
        }else if(quantity > stock){
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong...',
              text: `Insufficient stocks`,
            })
        }else{
            fetch(`https://capstone2v2-guiang.onrender.com/orders/addToCart/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                },
                body: JSON.stringify({
                    productId: productId_parameter,
                    quantity: quantity
                })
            }).then((result) => result.json())
            .then((data) => {
                if(data.isOk!==true){
                    Swal.fire({
                      icon: 'error',
                      title: 'Something went wrong...',
                      text: data.message,
                    })
                }else{
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: `Added ${name} (Qty: ${quantity}) to Cart.`,
                    });

                    navigate('/products');
                }
            });
        }
    }

    useEffect(()=>{
        fetch(`https://capstone2v2-guiang.onrender.com/products/getproduct/${prodID}`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((result) => result.json())
            .then((data) => {
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price.toFixed(2));
                setStock(data.availableStock);
                setImage(data.image.url);
                if(user.access !== null){
                    
                }
            });
        }, [prodID]);

	return(
		<Container className="mt-3 container-fluid">
            <Row>
                <Col lg={{ span: 6, offset: 4 }}>
                    <Card>
                        <Card.Body>
                            <Container>
                                <Row>
                                    <Col className='col-6'>
                                            <img src={image} className='img-fluid'/>
                                            <strong>{name}</strong>
                                        <Card.Text>Available Stock: {stock}</Card.Text>
                                    </Col>
                                    <Col className='col-6'>
                                        <Card.Subtitle>Description:</Card.Subtitle>
                                        <Card.Text className="pb-1">{description}</Card.Text>
                                        <Card.Subtitle>Price:</Card.Subtitle>
                                        <Card.Text className="pb-1">&#8369; {price}</Card.Text>
                                        <Card.Subtitle>Quantity:</Card.Subtitle>
                                        <Card.Text>
                                            <div className='text-center'>
                                                <h4>{quantity}</h4>
                                                <Button 
                                                    className='mx-1'
                                                    variant="danger"
                                                    onClick={() => {
                                         
                                                        if(quantity > 0){
                                                            setQuantity(quantity-1);
                                                        }
                                                    }}
                                                >-</Button>
                                                <Button 
                                                    className='mx-1 '
                                                    variant="success"
                                                    onClick={() => {
                                               
                                                        if(quantity < stock){
                                                            setQuantity(quantity+1);
                                                        }
                                                    }}
                                                >+</Button>
                                            </div>

                                        </Card.Text>
                                        <div className='text-center'>
                                            {
                                                (user.access !== null)  ?
                                                    <Button 
                                                        className=''
                                                        variant="primary"
                                                        onClick={() => {
                                               
                                                            addToCart(prodID)
                                                        }}
                                                    >Add to Cart</Button>
                                                :
                                                    <Button variant='danger'as={NavLink} to={'/login'} exact>
                                                        Please Login First
                                                    </Button>
                                            } 
                                        </div>  
                                    </Col>
                                </Row>
                            </Container>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}