import { useState, useEffect } from 'react';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

import Swal from 'sweetalert2';

export default function UserOrder() {

    const navigate = useNavigate();

    const [cart, setCart] = useState([]);
    const [quantity, setQuantity] = useState(0);
    const [totalQty, setTotalQty] = useState(0);
    const [totalOrder, setTotalOrder] = useState(0);
    const [proceed, setProceed] = useState(false);
    const [clickCtr, setClickCtr] = useState(0);
    
    const AddQuantity = (CartItem_Id, CurrentQuantity) =>{

    }

    const DeductQuantity = (CartItem_Id, CurrentQuantity) =>{

    }

    function toArchive(ProductId){

    }

    useEffect(() => {
        


    }, [])
    return(
        <>
            <h1 className="text-center my-4">Orders</h1>
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>Order ID</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {
                        (cart.length < 1) ?
                            <tr>
                                <td colSpan='5' className='text-center text-danger'>
                                    <h5 >
                                        No Items on Cart
                                    </h5>
                                </td>
                                   
                            </tr>
                        :
                            cart
                    }
                </tbody>
            </Table>


        </>

        )
}