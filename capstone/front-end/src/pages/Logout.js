import { Navigate } from 'react-router-dom';
import {useContext, useEffect} from 'react';

import UserContext from "../UserContext.js";

export default function Logout() {
	/*Get  Global Variables*/
	const {setUser, unsetUser} = useContext(UserContext);

	unsetUser();

	useEffect(() => {		
		setUser({
			/*id: null,
			isAdmin: null*/
			access:null
		});
	});

    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}