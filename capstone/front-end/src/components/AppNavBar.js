import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {NavLink} from 'react-router-dom';
import {useState, useContext} from 'react';
import React from 'react';
import UserContext from '../UserContext.js';

export default function AppNavBar(){
	//const [user, setUser] = useState(localStorage.getItem('token'));
	const { user } = useContext(UserContext);
	const [ isAdmin, setIsAdmin ] = useState(false);
	
	fetch(`https://capstone2v2-guiang.onrender.com/users/viewDetails/`, {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			'Authorization': `Bearer ${localStorage.getItem('myToken')}`
		}
	}).then((result) => result.json())
	.then((data) => {
		setIsAdmin(data.isAdmin);
	});

	return (
		<Navbar expand="lg" className='sticky-top bg-white mx-0'>
		    <Container>
				<Navbar.Brand as={NavLink} to="/" className='text-light'>
				<img src="https://www.swirlingovercoffee.com/wp-content/uploads/2019/05/Power-Mac-Center-Logo.jpg" className="mb-4" alt='logo' width="80" height="80" />
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="me-auto">
						{/*exact - useful for NavLinks to respond with exact path NOT partial match*/}
						<Nav.Link as={NavLink} to='/' className='text-dark' exact>Home</Nav.Link>
						<Nav.Link as={NavLink} to="/products" className='text-dark' exact>Products</Nav.Link>
						{
							(user.access !== null)  ? 	
								<React.Fragment>
									{/*FOR ADMINS*/}
									{
										(isAdmin === true) ? 
											<Nav.Link as={NavLink} to="/orders" className='text-dark' exact>User Orders</Nav.Link>	
										:
											<Nav.Link as={NavLink} to="/mycart" className='text-dark' exact>View Cart</Nav.Link>
									}
									<Nav.Link as={NavLink} to="/profile" className='text-dark' exact>My Account</Nav.Link>							
									<Nav.Link as={NavLink} to="/logout" className='text-dark' exact>Logout</Nav.Link>
								</React.Fragment>
							:
								//Fragments are use for multiple conditional rendering
								<React.Fragment>
									<Nav.Link as={NavLink} to="/login" className='text-dark' exact>Login</Nav.Link>
									<Nav.Link as={NavLink} to="/register" className='text-dark' exact>Register</Nav.Link>	
								</React.Fragment>			
						}											
					</Nav>
				</Navbar.Collapse>
		    </Container>
		</Navbar>
	);
}