import {Card, Button, Row, Col, Container, Table} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'
import UserContext from "../UserContext.js";
import Product_Stock_Deliveries from './Product_Stock_Deliveries.js';
import ViewOrder from './ViewOrder.js';

export default function ProductStockInfo({productProp}){
	const [data, setData] = useState([]);


	function displayDeliveries(){
		return (
			<Row className='pt-5'>
            	<div className='text-center py-3 rounded border shadow' >
	        		<h5>DELIVERED STOCKS</h5>
	        		<hr />
	        	  	<Table striped bordered hover responsive>
		        	    <thead>
		        	        <tr className="text-center " autoFocus >
		        	            <th>Date Delivered</th>
		        	            <th>Delivered By</th>
		        	            <th>Quantity</th>
		        	        </tr>
		        	    </thead>
		        	    <tbody>
			                	<Product_Stock_Deliveries datas={productProp.datas}/>
		        	    </tbody>
					</Table>
				</div>
            </Row>
		);
	}



	function displayReturns(){
		return (
			<Row className='pt-5'>
            	<div className='text-center py-3 rounded border shadow'>
	        		<h5>RETURNED STOCKS</h5>
	        		<hr />
	        	  	<Table striped bordered hover responsive>
		        	    <thead>
		        	        <tr className="text-center " autoFocus >
		        	            <th>Date Returned</th>
		        	            <th>Reason for Return</th>
		        	            <th>Quantity</th>
		        	        </tr>
		        	    </thead>
		        	    <tbody>
			                	<Product_Stock_Deliveries datas={productProp.retdatas}/>
		        	    </tbody>
					</Table>
				</div>
            </Row>
		);
	}


	function displaySold(){
		return (
			<Row className='pt-5'>
            	<div className='text-center py-3 rounded border shadow'>
	        		<h5>SOLD STOCKS</h5>
	        		<hr />
	        	  	<Table striped bordered hover responsive>
		        	    <thead>
		        	        <tr className="text-center " autoFocus >
		        	            <th>Order ID</th>
		        	            <th>Quantity</th>
		        	            <th>Price</th>
		        	            <th>Total</th>
		        	        </tr>
		        	    </thead>
		        	    <tbody>
		        	    	{
		        	    		productProp.soldData.toReversed().map(item => {
		        	    					return (
		        	    						<tr key={item.orderId}>
		        	    							<td className='text-center'>
		        	    								<strong>
		        	    									<ViewOrder order={item.orderId} />
		        	    								</strong>
		        	    							</td>
		        	    							<td>
		        	    								{item.quantity}
		        	    							</td>
		        	    							<td className='text-center'>
		        	    								&#8369; {item.price.toFixed(2)}
		        	    							</td>
		        	    							<td className='text-center'>
		        	    								&#8369; {(item.price * item.quantity).toFixed(2)}
		        	    							</td>
		        	    						</tr>
		        	    					);
		        	    				})
		        	    	}
		        	    </tbody>
					</Table>
				</div>
            </Row>
		);
	}







	
	return (
		<>
		<Row className='pt-5'>
				<Col className='mb-2 col-lg-4 col-md-6 col-sm-12'>		
					<Link className='LinkDecor'
					    onClick={() => {
					    	setData(displayDeliveries());
					    }}>
							<Card className='shadow h-100 text-center'>
							      <Card.Body>
							        <Card.Title>DELIVERED STOCKS</Card.Title>
							        <hr />
							        <Card.Subtitle><h4>{productProp.dels}</h4></Card.Subtitle>
							      </Card.Body>
							</Card>
					</Link>
				</Col>
				<Col className='mb-2 col-lg-4 col-md-6 col-sm-12'>
			        <Link className='LinkDecor' onClick={() => {
					    	setData(displayReturns());
					    }}>
							<Card className='shadow h-100 text-center'>
							    <Card.Body>
							    	<Card.Title>RETURNED STOCKS</Card.Title>
							        <hr />
							        <Card.Subtitle><h4>{productProp.rets}</h4></Card.Subtitle>
							    </Card.Body>
						    </Card>
					</Link>
				</Col>
			<Col className='mb-2 col-lg-4 col-md-6 col-sm-12'>
		        <Link className='LinkDecor text-center' onClick={() => {
					    	setData(displaySold());
					    }}>
					<Card className='shadow h-100'>
					      <Card.Body>
					        <Card.Title>SOLD STOCKS</Card.Title>
					        <hr />
					        <Card.Subtitle><h4>{productProp.sold}</h4></Card.Subtitle>
					      </Card.Body>
					    </Card>
				</Link>
			</Col>
		</Row>
		{data}
		</>
	);
}


// Checks if the CourseCard component is getting the correct prop types.
/*ProductStockInfo.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
};*/