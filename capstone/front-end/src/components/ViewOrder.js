import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function ViewOrder({order}){

	const [orderId, setOrderId] = useState(order);

	const [showEdit, setShowEdit] = useState(false);

	const [latestStatus, setLatestStatus] = useState("");
	const [latestUpdate, setLatestUpdate] = useState("");
	const [price, setPrice] = useState("");

	const [orderedItems, setOrderedItems] = useState([]);
	const [actions, setActions] = useState([]);
    const [totalQty, setTotalQty] = useState(0);
    const [totalOrder, setTotalOrder] = useState(0);

	const openEdit = (courseId) => {
		let QTYholder = 0, TOTHolder = 0;
		
		setShowEdit(true);

		fetch(`https://capstone2v2-guiang.onrender.com/orders/GetSingleOrder/${orderId}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('myToken')}`
			}
		}).then((result) => result.json())
		.then((data) => {
			setOrderId(data._id);
			setLatestStatus(data.actionsTaken[data.actionsTaken.length - 1].action);
			setLatestUpdate(data.actionsTaken[data.actionsTaken.length - 1].action_date);
			setOrderedItems(data.productsPurchased);
			setActions(data.actionsTaken);
			setPrice(data.price);

    		orderedItems.forEach(item => {

				QTYholder += item.quantity;
				TOTHolder += item.subtotal;
    		})

            setTotalQty(QTYholder);
            setTotalOrder(TOTHolder);
		});
	};

	const closeEdit = () => {
		setShowEdit(false);
		setLatestStatus("");
		setLatestUpdate("");
		setPrice("");
	};


	return (
		<>
			<Link 
			    className=''
			    onClick={() => {
			    	openEdit(order);
			    }}
			>
				{order}
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
				<Modal.Header closeButton>
					<Modal.Title>Order Details</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>
		        	<Container>
		        	    <Row>
		        	        <Col>
		        	            <Table>
		        	                <tbody>
		        	                    <tr>
		        	                        <td>
		        	                            <h5>
		        	                                Order ID: 
		        	                            </h5>
		        	                        </td>
		        	                        <td>
		        	                                {orderId}
		        	                        </td>
		        	                    </tr>
		        	                    <tr>
		        	                        <td>
		        	                            <h5>
		        	                                Recent Status: 
		        	                            </h5>
		        	                        </td>
		        	                        <td>
		        	                               {latestStatus}
		        	                        </td>
		        	                    </tr>
		        	                    <tr>
		        	                        <td>
		        	                            <h5>
		        	                                Latest Update: 
		        	                            </h5>
		        	                        </td>
		        	                        <td>
		        	                               {latestUpdate}
		        	                        </td>
		        	                    </tr>
		        	                </tbody>
		        	            </Table>
		        	        </Col>
		        	    </Row>
		        	    <Row>
		        	    	<Col>
		        	    		<Table striped bordered hover responsive>
		        	    		    <thead>
		        	    		        <tr className="text-center">
		        	    		            <th className='align-middle text-center'>Product Description</th>
		        	    		            <th className='align-middle text-center'>Quantity</th>
		        	    		            <th className='align-middle text-center'>Total Price</th>
		        	    		        </tr>
		        	    		    </thead>

		        	    		    <tbody>
		        	    		    	{
			        	    		    	orderedItems.map(item => {

		    		    	                    return (
		    		    	                        <tr key={item.productId}>
		    		    	                            <td>
		    		    	                                {item.name} @ &#8369;{item.price.toFixed(2)}
		    		    	                            </td>
		    		    	                            <td className='align-middle text-center'>
		    		    	                                    {item.quantity}
		    		    	                            </td>
		    		    	                            <td className='align-middle text-end'>&#8369;{item.subtotal.toFixed(2)}</td>
		    		    	                        </tr>
		    		    	                    );
		    		    	                })
		        	    		    	}
		        	    		    </tbody>
		        	    		</Table>
		        	    	</Col>
		        	    </Row>
		        	    <Row>
		                    <Col className='offset-md-6 col-6'>
		                        <Table>
		                            <tbody>
		                                <tr>
		                                    <td>
		                                            Total # of Items:
		                                    </td>
		                                    <td className='align-middle text-end'>
		                                            {totalQty}
		                                    </td>
		                                </tr>
		                                <tr>
		                                    <td>
		                                            Order Total:
		                                    </td>
		                                    <td className='align-middle text-end'>
		                                            &#8369;{totalOrder.toFixed(2)}
		                                    </td>
		                                </tr>
		                            </tbody>
		                        </Table>
		                    </Col>
                		</Row>
                		<Row>
                			<Col className=''>
	                			<ul class="timeline">
	                				{
	                					actions.toReversed().map(item => {
	                						return (
	                							<>
						                            <li class="event">
						                                <h3>{item.action_date}</h3>
						                                <h3>{item.action}</h3>
						                                <p>{item.remarks}</p>
						                            </li>
					                            </>
	                						);
	                					})
	                				}
		                        </ul>
                			</Col>
                		</Row>
		        	</Container>
		        </Modal.Body>
		        <Modal.Footer>
		        	<Button variant="secondary" onClick={()=>{
		        		closeEdit();
		        	}}>Close</Button>
		        </Modal.Footer>
			</Modal>
		</>
	);
}