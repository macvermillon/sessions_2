import {Row, Col, Card, Image} from 'react-bootstrap';
import '../App.css';


export default function Highlights() {

	return (
        <div classname="container-fluid mb-4 pb-5">
            
                <Row className="mt-3 mb-3">
                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Image src='https://powermaccenter.com/cdn/shop/files/Pay_It_Your_Way_x172.png' width='290' height='200'/>
                        <Card.Title>Easy Payments</Card.Title>
                        <Card.Text>
                        Choose from multiple installment option suitable for wallet
                        </Card.Text>
                    </Card.Body>
                    </Card>
                </Col>

                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Image src='https://powermaccenter.com/cdn/shop/files/Tap_Into_Savings_x172.png' width='290' height='200'/>
                        <Card.Title>TAP INTO SAVINGS</Card.Title>
                        <Card.Text>
                        Score up to ₱15,000 off on select models until December 31
                        </Card.Text>
                    </Card.Body>
                    </Card>
                </Col>

                <Col xs={12} md={4}>
                    <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Image src='https://powermaccenter.com/cdn/shop/files/Microsoft_Office_x172.png' width='290' height='200'/>
                        <Card.Title>PRODUCTIVITY PARTNER</Card.Title>
                        <Card.Text>
                        Do more and achieve more with Microsoft Office
                        </Card.Text>
                    </Card.Body>
                    </Card>
                </Col>
            </Row>

        </div>

		
	)
}







