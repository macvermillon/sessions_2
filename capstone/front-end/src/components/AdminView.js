import {Row, Container} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import ProductCards from './ProductCards.js';
import SearchBarAdmin from './SearchBarAdmin.js';
/*import CourseSearch from './CourseSearch';*/


export default function AdminView({productData}) {

    const [products, setProducts] = useState([])


    return(
        <>
            <Container fluid className='mt-2'>
                <SearchBarAdmin />
            </Container>
        </>
            )
}