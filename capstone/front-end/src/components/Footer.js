export default function Footer(){
	return (
		<div className='footer bg-dark p-1'>
			<p>&copy; {new Date().getFullYear()} PowerMacCenter</p>
		</div>
	);
}