import {Row, Container, Col, Card, Table, Modal, Button} from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext.js";
import {useParams, useNavigate, Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';


export default function Product_Stock_Deliveries({datas}) {
	const navigate = useNavigate();
    let { prodID } = useParams();

	const [showEdit, setShowEdit] = useState(false);
	const [display, setDisplay] = useState([]);

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) 
	        month = '0' + month;
	    if (day.length < 2) 
	        day = '0' + day;

	    return [month, day, year].join('-');
	}

    return(
        <>
		        	      	{
		        	      		datas.toReversed().map(item => {
		        	      					return (
		        	      						<tr key={item._id}>
		        	      							<td className='text-center'>
		        	      								{formatDate(item.deliveredOn || item.trashedOn)}
		        	      							</td>
		        	      							<td>
		        	      								{item.deliveredBy || item.reasonForTrashing}
		        	      							</td>
		        	      							<td className='text-center'>
		        	      								{item.quantity}
		        	      							</td>
		        	      						</tr>
		        	      					);
		        	      				})
		        	      	}
        </>
            );
}