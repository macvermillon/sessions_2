const dotenv = require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser')

/*app.use(express.json());
app.use(express.urlencoded({
	extended:true
}));*/

// Express 4.0 para ma save ang FILES converted to base64 string 
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));


app.use(cors());

/*Routes*/
app.use("/users", require('./routes/user_routes.js'));
app.use("/products", require('./routes/product_routes.js'));
app.use("/orders", require('./routes/order_routes.js'));

/*app.use("/courses", require('./routes/course_routes.js'));*/

mongoose.connect(process.env.MongoDB_ConnectionString, {
	useNewUrlParser : true,
	useUnifiedTopology : true
});
mongoose.connection.once("open", () => console.log("We're connected to MongoDB"));

/*Server Gateway Response*/
if(require.main === module){
	app.listen(process.env.PORT, () => {
		console.log(`API is now running on port ${process.env.PORT}`);
	});
}

module.exports = app;



/*Initial Commit from new Laptop*/