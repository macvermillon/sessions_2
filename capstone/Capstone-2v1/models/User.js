const mongoose = require("mongoose");

module.exports = mongoose.model("User", new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	contact: {
		type: Number,
		required: [true, 'contact is required!']
	},
	address: {
		type: String,
		required: [true, 'address is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	userType: {
		type: Number,
		/*1 - User, 2 - Delivery*/
		default: 1
	},
	isDisabled: {
		type: Boolean,
		default: false
	},
	myCart: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required!']
			},
			quantity: {
				type: Number,
				required: [true, 'Qty is required!']
			},
			price: {
				type: Number,
				required: [true, 'Price is required!']
			}
		}
	]
}));