const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');

const auth = require('../auth.js');
const bcrypt = require('bcrypt');
var mongoose = require('mongoose');


module.exports.setOrder = (request, response) => {
	let orderID = request.body.orderId;
	let action = request.body.action;
	let remarks = request.body.remarks;

	return Order.findById(orderID).then((result) => {
		let newItem = {
			action: action,
			remarks: remarks
		};
		result.actionsTaken.push(newItem);
		return result.save().then((result, error) => {
			if(error){
				return response.send({
					isOk: false,
					message: error
				});
			}else{
				return response.send({
					isOk: true,
					message: `${orderID} successfully updated.`
				})
			}
		}).catch(err => response.send(err));
	});
}

/*Add to Cart*/
module.exports.addToCart = (request, response) => {
	const productID = request.body.productId;
	let quantity = request.body.quantity;
	return Product.findById(productID).then((result, error) => {
		let objectHolder = {
			ar_productId: result._id,
			ar_qty: request.body.quantity,
			ar_price: result.price
		};

		return User.findById(request.user.id).then((result) => {
			let newItem = {
				productId: objectHolder.ar_productId,
				quantity: objectHolder.ar_qty,
				price: objectHolder.ar_price
			};
			
			let isUpdated = false;
			result.myCart.forEach((items) => {
				let ProdID = items.productId;
				if(ProdID == productID){
					items.quantity = items.quantity + quantity;
					isUpdated = true;
					//break;
				}
			});

			if(!isUpdated){
				result.myCart.push(newItem);
			}

			return result.save().then((result, error) => {
				if(error){
					return response.send({
						isOk: false,
						message: error
					});
				}else{
					return response.send({
						isOk: true,
						message: `${productID} (${quantity}) successfully added to cart.`
					})
				}
			}).catch(err => response.send(err));
		});
	}).catch(err => err);
};


/*Check-Out User*/
module.exports.CheckOut = async (request, response, next) => {
	if(request.user.isAdmin){
		return response.send({
						isOk: false,
						message: 'Only non-admin accounts are allowed.'
					});
	}
	let GetAllItemsOnCart =  await User.findById(request.user.id).then((result, error) => {
		let returnArray = [];
		result.myCart.forEach((item) => {
			returnArray.push({
				productID: item.productId,
				quantity: item.quantity,
				price: item.price
			});
		});
		return returnArray;
		});

	if(GetAllItemsOnCart.length < 1){
		return response.send({
						isOk: false,
						message: "No Item on Cart"
					});
	}

	let newOrder = new Order({
			userId: request.user.id,
			productsPurchased: GetAllItemsOnCart,
			actionsTaken: [{
				action: "Pending",
				remarks: "Order Placed by User"
			}]
		});

		return newOrder.save().then((savedTask, saveErr) => {
			if(saveErr){
				return response.send({
						isOk: false,
						message: `Order not saved! Please try again.`
					});
			}else{
				next();
			}
		}).catch(err => response.send(err));
};

/*Deleting all items on cart after Check-Out*/
module.exports.AfterCheckOut = (request, response) => {
	return User.findOneAndUpdate({
		_id: request.user.id
	}, 
	{
		$pull: {
			myCart: {
				productId: {
					$ne: ""
				}
			}
		}
	}).then((result, error) => {
		
		//result.enrollments.pop();
		return result.save().then((result, error) => {
			if(error){
				return response.send({
						isOk: false,
						message: error
					});
			}else{
				return response.send({
						isOk: true,
						message: `Order saved successfully!`
					});
			}
		}).catch(err => response.send(err));
	});
};


module.exports.MyCart = async (request, response) => {
	if(request.user.isAdmin){
		return response.send({
						isOk: false,
						message: 'Only non-admin accounts are allowed.'
					});
	}
	let ProductInfo = await Product.find({}).then((result) => {
		return result;
	});

	let GetMyInfo = await User.findById(request.user.id).then((result) => {
		return result;
	});

	let MyCartDisplay = [];

	GetMyInfo.myCart.forEach((items) => {
		ProductInfo.forEach((PrdInfo) => {
			if(PrdInfo._id == items.productId){
				MyCartDisplay.push({
					productId: PrdInfo._id,
					name: PrdInfo.name,
					quantity: items.quantity,
					price: items.price,
					/*subtotal: (items.price * items.quantity)*/
					subtotal: (items.price * items.quantity)
				})
			}
		})
	});
	
	let totalprice = 0;
	/*display total price*/
	MyCartDisplay.forEach((item) => {
		totalprice += (item.quantity * item.price);
	});

	/*MyCartDisplay.push({
		totalprice: totalprice.toFixed(2)
	});*/


	return response.send(MyCartDisplay);
};

/*Edit Quantity*/
module.exports.EditQuantity = (request, response) => {
	const productID = request.body.productId;
	let quantity = request.body.quantity;
	return User.findById(request.user.id).then((result) => {

		let isUpdated = false;
		result.myCart.forEach((items) => {
			let ProdID = items.productId;
			if(ProdID == productID){
				items.quantity = quantity;
				isUpdated = true;
				//break;
			}
		});

		if(!isUpdated){
			return response.send({
				isOk: false,
				message: "Product Not Fount in Cart"
			});
		}

		return result.save().then((result, error) => {
			if(error){
				return response.send({
					isOk: false,
					message: error
				});
			}else{
				return response.send({
					isOk: true,
					message: quantity + "added"
				});
			}
		}).catch(err => response.send(err));
	});
};

/*Remove from Cart*/
module.exports.RemoveProduct = (request, response) => {
	let productId_ = request.body.productId;
	let UserID = request.user.id;

	return User.findByIdAndUpdate(UserID, {
		$pull: {
			myCart: {
				productId: productId_
			}
		}
	}).then((result, saveErr) => {
		if(saveErr){
			return response.send({
					isOk: false,
					message: `Item not Removed! Please try again.`
				});
		}else{
			return response.send({
					isOk: true,
					message: `${productId_}'s successfully removed from your cart.`
				});
		}
	});
};

/*Retrieve all orders (Admin only)*/
module.exports.GetAllOrders = async (request, response) => {
	let ProductInfo = await Product.find({}).then((result) => {
		return result;
	});

	let GetMyInfo = await User.find({}).then((result) => {
		return result;
	});

	let OrderInfo = await Order.find({
		isActive: true
	}).then((result) => {
		return result;
	});

	let OrderList = [];
	let userName = "";

	OrderInfo.forEach((item)=>{
		let OrderItems = [];
		let totalprice = 0;
		item.productsPurchased.forEach((Inside_Users_Items) => {
			ProductInfo.forEach((ProductInfo_items) => {
				if(ProductInfo_items._id == Inside_Users_Items.productID){
					OrderItems.push({
						productId: ProductInfo_items._id,
						name: ProductInfo_items.name,
						quantity: Inside_Users_Items.quantity,
						price: Inside_Users_Items.price,
						subtotal: (Inside_Users_Items.price * Inside_Users_Items.quantity).toFixed(2)	
					})
				}
			});
		});
		OrderItems.forEach((item) => {
			totalprice += (item.quantity * item.price);
		});

		OrderItems.push({
			totalprice: totalprice.toFixed(2)
		});

		GetMyInfo.forEach((GetMyInfo_items) => {
			if(GetMyInfo_items._id == item.userId){
				userName = GetMyInfo_items.firstName +" "+ GetMyInfo_items.lastName;
			}
		});

		

		

		OrderList.push({
			_id: item._id,
			userId: item.userId,
			name: userName,
			purchasedOn: item.purchasedOn,
			productsPurchased: OrderItems			
		})
	});
	return response.send(OrderList)
	//return response.send(GetMyInfo)
	//return response.send(OrderInfo)
}

module.exports.GetOrdersPerUser = async (request, response) => {
	if(request.user.isAdmin){
		return response.send("Only non-admins are allowed.");
	}

	let ProductInfo = await Product.find({}).then((result) => {
		return result;
	});

	let GetMyInfo = await User.find({}).then((result) => {
		return result;
	});

	let OrderInfo = await Order.find(
		{
			userId: request.user.id
		}
	).then((result) => {
		return result;
	});

	let OrderList = [];
	let userName = "";

	OrderInfo.forEach((item)=>{
		let OrderItems = [];
		let totalprice = 0;
		item.productsPurchased.forEach((Inside_Users_Items) => {
			ProductInfo.forEach((ProductInfo_items) => {
				if(ProductInfo_items._id == Inside_Users_Items.productID){
					OrderItems.push({
						productId: ProductInfo_items._id,
						name: ProductInfo_items.name,
						quantity: Inside_Users_Items.quantity,
						price: Inside_Users_Items.price,
						subtotal: (Inside_Users_Items.price * Inside_Users_Items.quantity).toFixed(2)	
					})
				}
			});
		});
		OrderItems.forEach((item) => {
			totalprice += (item.quantity * item.price);
		});

		OrderItems.push({
			totalprice: totalprice.toFixed(2)
		});

		GetMyInfo.forEach((GetMyInfo_items) => {
			if(GetMyInfo_items._id == item.userId){
				userName = GetMyInfo_items.firstName +" "+ GetMyInfo_items.lastName;
			}
		});

		

		

		OrderList.push({
			_id: item._id,
			purchasedOn: item.purchasedOn,
			productsPurchased: OrderItems,
			actionsTaken: item.actionsTaken			
		})
	});
	return response.send(OrderList)
};

/*Deleting all items on cart*/
module.exports.ClearCart = (request, response) => {
	return User.findOneAndUpdate({
		_id: request.user.id
	}, 
	{
		$pull: {
			myCart: {
				productId: {
					$ne: ""
				}
			}
		}
	}).then((result, error) => {
		
		//result.enrollments.pop();
		return result.save().then((result, error) => {
			if(error){
				return response.send(error);
			}else{
				return response.send(`Your cart is now empty.`);
			}
		}).catch(err => response.send(err));
	});
};

/*Remove from Order*/
module.exports.RemoveOrder = (request, response) => {
	let productId_ = request.body.orderId;
	let UserID = request.user.id;

	return Order.findByIdAndUpdate(productId_, {
		isActive: false
	}).then((result, saveErr) => {
		if(saveErr){
			return response.send(`Order not Removed! Please try again.`);
		}else{
			return response.send(`${productId_} successfully removed from your list of orders.`);
		}
	});
};



module.exports.GetSingleOrder = async (request, response) => {
	let orderId = request.params.orderId;

	let ProductInfo = await Product.find({}).then((result) => {
		return result;
	});

	let GetMyInfo = await User.find({}).then((result) => {
		return result;
	});

	let OrderInfo = await Order.findById(orderId).then((result) => {
		return result;
	});

	let OrderList = [];
	let userName = "";

	let OrderItems = [];
	let totalprice = 0;
	OrderInfo.productsPurchased.forEach((Inside_Users_Items) => {
		ProductInfo.forEach((ProductInfo_items) => {
			if(ProductInfo_items._id == Inside_Users_Items.productID){
				OrderItems.push({
					productId: ProductInfo_items._id,
					name: ProductInfo_items.name,
					quantity: Inside_Users_Items.quantity,
					price: Inside_Users_Items.price,
					subtotal: (Inside_Users_Items.price * Inside_Users_Items.quantity)	
				})
			}
		});
	});
	OrderItems.forEach((item) => {
		totalprice += (item.quantity * item.price);
	});

	/*OrderItems.push({
		totalprice: totalprice.toFixed(2)
	});*/

	GetMyInfo.forEach((GetMyInfo_items) => {
		if(GetMyInfo_items._id == OrderInfo.userId){
			userName = GetMyInfo_items.firstName +" "+ GetMyInfo_items.lastName;
		}
	});
	return response.send({
			_id: OrderInfo._id,
			purchasedOn: OrderInfo.purchasedOn,
			productsPurchased: OrderItems,
			actionsTaken: OrderInfo.actionsTaken			
		})
};