const User = require('../models/User.js');
//const Course = require('../models/Course.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
var mongoose = require('mongoose');

/*Check if email is already registered*/
module.exports.checkIfRegisteredEmail = (request, response, next) => {
	return User.find({
			email: request.body.email
		}).then((result, error) => {
			if(result.length < 1){
				next();
			}else{
				return response.send({
					isOK: false,
					message: "Email already registered..."
				});
			}
		});
};

/*Register User*/
module.exports.register = (request, response) => {
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		contact: request.body.contact,
		address: request.body.address,
		password: bcrypt.hashSync(request.body.password, 10),
	});

	return newUser.save().then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
				isOK: false,
				message: "Something went wrong..."
			});
		}else{
			return response.send(true);
		}
	}).catch(err => response.send(err));
};

/*Login*/
module.exports.login = (request, response) => {
	let email = request.body.email;
	let password = request.body.password;
	let errorMsg = [];

	if(email === null || email === undefined || email === ""){
		errorMsg.push("Email is Empty");
	}

	if(password === null || password === undefined || password === ""){
		errorMsg.push("Password is Empty");
	}

	if(errorMsg.length < 1){
		return User.findOne({
				email: request.body.email
			}).then((result, error) => {
				if(result == null){
						return response.send({
							isLogin: false,
							message: "User not found"
						});
				}else{
					const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
					if(isPasswordCorrect){
						return response.send({
							myToken: auth.createAccessToken(result)
						})
					}else{
						return response.send({
							isLogin: false,
							message: "Wrong Password!"
						});
					}
				}
			});
	}else{
		response.send(errorMsg);
	}
};

/*Update User Info*/
module.exports.updateUserInfo = (request, response) => {
	const newFirstName = request.body.firstName;
	const newLastName = request.body.lastName;
	const newContact = request.body.contact;
	const newAddress = request.body.address;
	const userId = request.user.id;

	return User.findByIdAndUpdate(userId, {
		firstName: newFirstName,
		lastName: newLastName,
		contact: newContact,
		address: newAddress


	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
				isOk: false,
				message: `User not updated! Please try again.`
			});
		}else{
			return response.send({
				isOk: true,
				message: `${request.user.email}'s information was successfully updated.`
			});
		}
	}).catch(err => response.send(err));
};

/*Check Password for Update*/
module.exports.checkPassword = (request, response, next) => {
	const oldPass = request.body.oldPassword;
	const newPass = request.body.newPassword;
	const retypePass = request.body.retypePassword;
	const userId = request.user.id;
	let errorMsg = [];



	return User.findById(userId).then((result, error) => {
		if(result.length < 1){
			return result.send({
				isOk: false,
				message: "Account no longer found."
			});
		}else{
			let retrievedPassword = result.password;
			const isPasswordCorrect = bcrypt.compareSync(oldPass, retrievedPassword);
			
			if(oldPass === null || oldPass === undefined || oldPass === ""){
				errorMsg.push("Old Password is Empty");
			}else if(!isPasswordCorrect){
				errorMsg.push("Old Password is incorrect.");
			}

			if(newPass === null || newPass === undefined || newPass === ""){
				errorMsg.push("New Password is Empty");
			}

			if(retypePass === null || retypePass === undefined || retypePass === ""){
				errorMsg.push("Re-type Password is Empty");
			}else if (retypePass !== newPass){
				errorMsg.push("Password does not match");
			}
			if(errorMsg.length < 1){
				next();
			}else{
				response.send({
					isOk: false,
					message: errorMsg
				});
			}

		}
	});
};

/*Change Passowrd*/
module.exports.updatePassword = (request, response) => {
	const newPass = request.body.newPassword;
	const userId = request.user.id;
	console.log(userId + "HAHAHAHHA");
	return User.findByIdAndUpdate(userId, {
		password: bcrypt.hashSync(newPass, 10)
	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
					isOk: false,
					message: `User not updated! Please try again.`
				});
		}else{
			return response.send({
					isOk: true,
					message: `${request.user.email}'s password was successfully updated.`
				});
		}
	}).catch(err => err);
};


/*View User Details*/
module.exports.viewDetails = (request, response) => {
	return User.findById(request.user.id).then((result, error) => {
			if(result === null){
				return response.send({
					isOk: false,
					message: "Email not found"
				});
			}else{
				let objectHolder = {
					_id: result._id,
					firstName: result.firstName,
					lastName: result.lastName,
					email: result.email,
					password: "HIDDEN",
					isAdmin: result.isAdmin,
					userType: result.userType,
					myCart: result.myCart.length + " item(s) on cart.",
					address: result.address,
					contact: result.contact
				}
				response.send(objectHolder);
			}
		});
};



/*Update User to Admin*/
module.exports.SetAdmin = (request, response) => {
	const userId = request.body.userId;

	return User.findByIdAndUpdate(userId, {
		isAdmin: true
	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send(`User not updated! Please try again.`);
		}else{
			return response.send(`${userId} has been set as Admin.`);
		}
	}).catch(err => err);
};