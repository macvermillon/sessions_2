const express = require("express");
const router = express.Router();
const userController = require("../controllers/user_controllers.js");
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = auth;

/*Register User*/
router.post('/register',userController.checkIfRegisteredEmail, userController.register);

/*Login*/
router.post('/login',userController.login);

/*Update User Info*/
router.put('/updateUser',verify, userController.updateUserInfo);

/*Update User Password*/
router.put('/updatePassword',verify, userController.checkPassword, userController.updatePassword);

/*View User Details*/
router.get('/viewDetails',verify, userController.viewDetails);

/*Update User Info*/
router.put('/setAdmin',verify, verifyAdmin, userController.SetAdmin);

module.exports = router;