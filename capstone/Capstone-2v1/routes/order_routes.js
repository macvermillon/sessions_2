const express = require("express");
const router = express.Router();
const productController = require("../controllers/order_controller.js");
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = auth;

/*Show All Product*/
router.post('/addToCart', verify, productController.addToCart);

/*Check-Out*/
router.post('/checkout', verify, productController.CheckOut, productController.AfterCheckOut);

/*My Cart*/
router.get('/mycart', verify, productController.MyCart);

/*Change Quantity*/
router.post('/changeQuantity', verify, productController.EditQuantity);

/*Remove From Cart*/
router.put('/removeItem', verify, productController.RemoveProduct);

/*Retrieve all orders (Admin only)*/
router.get('/viewAll', verify, verifyAdmin, productController.GetAllOrders);

/*Retrieve orders (Non-Admin only)*/
router.get('/viewOrders', verify, productController.GetOrdersPerUser);

/*Clear All Items From Cart*/
router.put('/clearCart', verify, productController.ClearCart);

/*Remove From Order*/
router.put('/archiveOrder', verify, verifyAdmin, productController.RemoveOrder);

/*Remove From Order*/
router.put('/setOrder', verify, verifyAdmin, productController.setOrder);

/*Remove From Order*/
router.get('/GetSingleOrder/:orderId', verify, productController.GetSingleOrder);


module.exports = router;