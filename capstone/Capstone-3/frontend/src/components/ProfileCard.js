import { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
// import ResetPassword from '../components/ResetPassword';
// import UpdateProfileForm from '../components/UpdateProfile';
function ProfileCard() {
  const [avatar, setAvatar] = useState('');
  const [address, setAddress] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');

  useEffect(() => {
    fetch(`http://localhost:4000/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== 'undefined') {
          setAvatar(data.avatar);
          setName(`${data.firstName} ${data.lastName}`);
          setEmail(data.email);
          setMobile(data.contactNumber);
          setAddress(`Brgy. ${data.address.brgy}, ${data.address.city}, ${data.address.province}`);
        }
      });
  }, []);

  return (
    <>
      <Row>
        <Col className="py-5 col-12 col-lg-6 offset-lg-3">
          <Card className="px-5 bg-light">
            <h2 className="mt-4 text-uppercase">Your Profile</h2>
            <img src={avatar} alt="avatar.png" width="120" className="img-fluid d-block" />
            <h2 className="mt-3">{`${name}`}</h2>
            <ul className="list-group mb-3">
              <h4>Contacts</h4>
              <li className="list-group-item list-group-item-action">Email: {email}</li>
              {/* <li>Mobile No: 09266772411</li> */}
              <li className="list-group-item list-group-item-action">Mobile No: {mobile}</li>
            </ul>
            <ul className="list-group mb-3">
              <h4>Address</h4>
              <li className="list-group-item list-group-item-action">{address}</li>
            </ul>
          </Card>
        </Col>
      </Row>
      <Row className="pt-4 mt-4">
        <Col>{/* <ResetPassword /> */}</Col>
      </Row>
      <Row className="pt-4 mt-4">
        <Col>{/* <UpdateProfileForm /> */}</Col>
      </Row>
    </>
  );
}

export default ProfileCard;
