import React from 'react';

const Footer = () => (
  <footer className="page-footer font-small blue pt-4 bg-light mt-3">
    <div className="container-fluid text-md-left">
      <div className="row ">
        <div className="col-md-6 mt-md-0 mt-3 mb-3 text-center">
          <img
            width="180"
            className="img-fluid"
            src={require('../images/logos/flaskhub.png')}
            alt="flaskhub_logo.png"
          />
        </div>

        <hr className="clearfix w-100 d-md-none pb-0" />

        <div className="col-md-3 mb-md-0 mb-3">
          <h5 className="text-uppercase">
            <strong>SITEMAP</strong>
          </h5>
          <ul className="list-unstyled footer-link">
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/products">Products</a>
            </li>
            <li>
              <a href="/">About Us</a>
            </li>
            <li>
              <a href="/contact">Contact Us</a>
            </li>
          </ul>
        </div>

        <div className="col-md-3 mb-md-0 mb-3">
          <h5 className="text-uppercase">
            <strong>Find us!</strong>
          </h5>
          {/* <ul className="list-unstyled">
            <li>
              <a href="#!">Link 1</a>
            </li>
            <li>
              <a href="#!">Link 2</a>
            </li>
            <li>
              <a href="#!">Link 3</a>
            </li>
            <li>
              <a href="#!">Link 4</a>
            </li>
          </ul> */}
          <div className="map-responsive shadow rounded me-3">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.909502025072!2d121.04137527543887!3d14.604230885882558!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7d7768f0f2f%3A0x83d47a3e6871279c!2sBurger%20King!5e0!3m2!1sen!2sph!4v1696944507425!5m2!1sen!2sph"
              width="800"
              height="600"
              allowFullScreen={false}
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
              title="Maps"
            ></iframe>
          </div>
        </div>
      </div>
    </div>

    <div className="footer-copyright text-center py-3">
      © 2020 Copyright:
      <a href="http://localhost:4001/"> flaskhub.com</a>
    </div>
  </footer>
);

export default Footer;
