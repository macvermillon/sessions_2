import {Row, Col, Card} from 'react-bootstrap';
import '../App.css';


export default function Highlights() {

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="card">
				  <Card.Body>
				    <Card.Title>Power Mac Center</Card.Title>
				    <Card.Text>
					 has evolved into a lifestyle shopping destination bringing you the finest in premium accessories, from exclusive audio devices, sleek cases, to the latest enhancements. Whether you’re into creativity, fashion, fitness, or adventure, there’s a perfect product for you in our stores.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="cardTwo">
				  <Card.Body>
				    <Card.Title>Have the complete Apple experience with our</Card.Title>
				    <Card.Text>
					• Apple Premium Reseller
					• Apple Authorized Reseller
					• Apple Authorized Service    	Provider
					• Apple Authorized Education Reseller
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="cardThree">
				  <Card.Body>
				    <Card.Title>Service</Card.Title>
				    <Card.Text>
					Power Mac Center also operates Apple Authorized and Premium Service Providers that give the best repair and maintenance services.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}







