import { useEffect, useState } from 'react';
import { Card, Col, Form, Row } from 'react-bootstrap';

import geoData from '../data/geoData';

function EditProfile() {
  const [avatar, setAvatar] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [brgy, setBrgy] = useState('');
  const [city, setCity] = useState('');
  const [province, setProvince] = useState('');

  useEffect(() => {
    fetch(`http://localhost:4000/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== 'undefined') {
          setAvatar(data.avatar.secure_url);
          setName(`${data.firstName} ${data.lastName}`);
          setEmail(data.email);
          setMobile(data.contactNumber);
          setBrgy(data.address.brgy);
          setCity(data.address.city);
          setProvince(data.address.province);
        }
      });
  }, []);

  const [cities, setCities] = useState([]);
  const [brgys, setBrgys] = useState([]);
  const provChange = (e) => {
    setProvince(e.target.value);
    setCities(geoData.find((cty) => cty.name === e.target.value).city);
    setBrgy([]);
  };

  const cityChange = (e) => {
    setCity(e.target.value);
    setBrgys(cities.find((cty) => cty.name === e.target.value).brgy);
  };

  return (
    <>
      <Row>
        <Col className="py-5 col-12 col-lg-6 offset-lg-3">
          <Card className="px-5 bg-light">
            <Form>
              <h2 className="mt-4 text-uppercase">Your Profile</h2>
              <img src={avatar} alt="avatar.png" width="120" className="img-fluid d-block" />
              <h2 className="mt-3">{`${name}`}</h2>
              <ul className="list-group mb-3">
                <h4>Contacts</h4>
                <li className="list-group-item list-group-item-action">Email: {email}</li>
                {/* <li>Mobile No: 09266772411</li> */}
                <li className="list-group-item list-group-item-action">Mobile No: {mobile}</li>
              </ul>
              <ul className="list-group mb-3">
                <h4>Address</h4>
                <li className="list-group-item list-group-item-action">
                  <Form.Group className="mb-2">
                    <Form.Label>Address: </Form.Label>
                    <Form.Select
                      className="mb-3"
                      aria-label="Default select example"
                      value={province}
                      onChange={(e) => provChange(e)}
                      required
                    >
                      <option> -- select province -- </option>
                      {geoData.map((prv) => (
                        <option value={prv.name}>{prv.name}</option>
                      ))}
                    </Form.Select>
                    <Form.Select
                      className="mb-3"
                      aria-label="Default select example"
                      value={city}
                      onChange={(e) => cityChange(e)}
                      required
                    >
                      <option> -- select city/municipality -- </option>
                      {cities.map((city) => (
                        <option value={city.name}>{city.name}</option>
                      ))}
                    </Form.Select>
                    <Form.Select
                      className="mb-3"
                      aria-label="Default select example"
                      value={brgy}
                      onChange={(e) => {
                        setBrgy(e.target.value);
                      }}
                      required
                    >
                      <option> -- select barangay -- </option>
                      {brgys.map((bry) => (
                        <option value={bry}>{bry}</option>
                      ))}
                    </Form.Select>
                  </Form.Group>
                </li>
              </ul>
            </Form>
          </Card>
        </Col>
      </Row>
      <Row className="pt-4 mt-4">
        <Col>{/* <ResetPassword /> */}</Col>
      </Row>
      <Row className="pt-4 mt-4">
        <Col>{/* <UpdateProfileForm /> */}</Col>
      </Row>
    </>
  );
}

export default EditProfile;
