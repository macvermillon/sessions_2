import { useEffect, useState } from 'react';
import { Card, Col } from 'react-bootstrap';

function OrderList() {
  const [orders, setOrders] = useState([]);
  const [error, setError] = useState(false);

  useEffect(() => {
    fetch(`http://localhost:4000/orders/view`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (data.state === false) {
          setError(true);
        } else {
          setOrders(data.orders);
        }
      })
      .catch((error) => console.log(error));
  }, []);
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 10;
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const records = orders.slice(firstIndex, lastIndex);
  const npage = Math.ceil(orders.length / recordsPerPage);
  const numbers = [...Array(npage + 1).keys()].slice(1);

  const prePage = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const changeCPage = (id) => {
    setCurrentPage(id);
  };

  const nextPage = () => {
    if (currentPage === lastIndex) {
    } else {
      setCurrentPage(currentPage + 1);
    }
  };
  return (
    <>
      {error === false ? (
        records.map((ord, i) => (
          <Col xs="12" lg={{ offset: 2, span: 8 }} className="mb-4">
            <Card className="cardHighlight">
              {/* <Card.Img variant="top" src={require('../images/logos/flaskhub.png')} /> */}
              <Card.Body>
                <Card.Title>
                  <strong>Order# {ord._id.slice(0, 8)}</strong>
                </Card.Title>
                <Card.Text>
                  {ord.products.map((prd) => {
                    const prodList = (
                      <Card.Text>
                        {prd.quantity} x <strong>{prd.name}</strong> @ Php {prd.subTotal / prd.quantity}/ea
                      </Card.Text>
                    );
                    return prodList;
                  })}
                </Card.Text>
                <Card.Text>{ord.description}</Card.Text>
                <Card.Text>
                  <h5>
                    <strong>Php {ord.totalAmount}</strong>
                  </h5>
                </Card.Text>
                <Card.Text>
                  <h6>
                    <strong>Status: </strong>
                    {ord.status}
                  </h6>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))
      ) : (
        <h1 className="text-center">No Orders Found</h1>
      )}
      <footer>
        <ul className="pagination">
          <li className="page-item">
            <a href="#/" className="page-link" onClick={prePage}>
              Prev
            </a>
          </li>

          {numbers.map((n, i) => (
            <li className={`page-item ${currentPage === n ? 'active' : ''}`} key={i}>
              <a href="#/" className="page-link" onClick={() => changeCPage(n)}>
                {n}
              </a>
            </li>
          ))}
          <li className="page-item">
            <a href="#/" className="page-link" onClick={() => nextPage()}>
              Next
            </a>
          </li>
        </ul>
      </footer>
    </>
  );
}

export default OrderList;
