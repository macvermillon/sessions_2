import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function SingleCheckOut({ productId }) {
  const navigate = useNavigate();
  const checkOut = () => {
    fetch(`http://localhost:4000/orders/${productId}/checkout`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.state === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#f8981e',
          });
          navigate('/');
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'Error',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };
  return (
    <>
      <Button className="login-btn float-end" onClick={() => checkOut(productId)}>
        Purchase
      </Button>
    </>
  );
}
export default SingleCheckOut;
