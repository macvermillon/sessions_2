import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';

function CartCount() {
  // State that will be used to store the products retrieved from the database
  const [cartCount, setCartCount] = useState(0);

  useEffect(() => {
    fetch(`http://localhost:4000/cart/viewCart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.state === false) {
          setCartCount(0);
        } else {
          setCartCount(data.cart.length);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <FontAwesomeIcon icon={faCartShopping} size="xl" /> {cartCount}
    </>
  );
}

export default CartCount;
