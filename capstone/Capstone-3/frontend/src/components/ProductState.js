import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

function ProductState({ product, isActive }) {
  const archiveToggle = (productId) => {
    fetch(`http://localhost:4000/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.state === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#f8981e',
          }).then(function () {
            window.location = '/dashboard';
          });
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'Error',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };

  const activateToggle = (productId) => {
    fetch(`http://localhost:4000/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.state === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#f8981e',
          }).then(function () {
            window.location = '/dashboard';
          });
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'Error',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button variant="danger" className="circle-btn" size="sm" onClick={() => archiveToggle(product)}>
          Archive
        </Button>
      ) : (
        <Button variant="success" className="circle-btn" size="sm" onClick={() => activateToggle(product)}>
          Activate
        </Button>
      )}
    </>
  );
}

export default ProductState;
