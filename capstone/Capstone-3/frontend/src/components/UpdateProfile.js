import React, { useState } from 'react';
import {Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UpdateProfileForm = ({fetchDetails}) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    // Update the profile API endpoint
    const apiUrl = `http://localhost:4000/users/profile`;
    const token = localStorage.getItem('token');

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT', // Use 'PUT' for updating the profile
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          firstName,
          lastName,
          mobileNo,
        }),
      });

      if (response.ok) {
          Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'User details Successfully Updated!'
      })
        console.log('Profile updated successfully!');
        refreshInput();
        fetchDetails();
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Please try again'
        })
        console.error('Failed to update profile.');
        refreshInput();
        fetchDetails();
      }
    } catch (error) {
      console.error('Error occurred while updating profile:', error);
    }
  };

  const refreshInput = () => {
    setFirstName('');
    setLastName('');
    setMobileNo('');
  }

  return (
    <div id="updateP">
    <Form onSubmit={handleFormSubmit} className="container" id="updateProfile">
      <h1>Update Profile</h1>
      <div className="form-group">
        <label htmlFor="firstName">First Name</label>
        <input
          type="text"
          className="form-control"
          id="firstName"
          name="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </div>

      <div className="form-group">
        <label htmlFor="lastName">Last Name</label>
        <input
          type="text"
          className="form-control"
          id="lastName"
          name="lastName"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </div>

      <div className="form-group mt-3">
        <label htmlFor="mobileNo">Mobile Number</label>
        <input
          type="tel"
          className="form-control"
          id="mobileNo"
          name="mobileNo"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </div>
      <Button type="submit" className="btn btn-primary mt-4 mb-4" id="updateP">Update Profile</Button>
    </Form>
    </div>
  );
};

export default UpdateProfileForm;