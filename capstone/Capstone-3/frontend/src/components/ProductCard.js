import	{ useState, useEffect } from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard ({productProp}) {

	// Destructured the courseProp object to access its properties.
	const { _id, name, description, price } = productProp;


	return (

		<Row>
			<Col className="pt-5 container">
				<Card className="cardHighlight p-3" id="productCard">
				      <Card.Body>
				      	<img src="https://powermaccenter.com/cdn/shop/files/MacBook_Pro_16_in_Silver_PDP_Image_Position-1__en-US_993cdae5-8332-463f-b1cf-ff3a12e7fa87_550x.jpg" className="mb-4" alt="profile-pic" width="200" height="200" />
				      	<Card.Title>{name}</Card.Title>
				      	<Card.Subtitle>Description:</Card.Subtitle>
				      	<Card.Text>{description}</Card.Text>
				      	<Card.Subtitle>Price:</Card.Subtitle>
				      	<Card.Text>Php {price}</Card.Text>
				      	<Button as={Link} to={`/product/${_id}`} variant="primary" id="viewDetails">View Details</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>

	)
};