import { useState, useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../context/UserContext';
import '../App.css';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const [isCollapsed, setIsCollapsed] = useState(false);

  const handleToggle = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <Navbar expand="lg" className="bg-body-tertiary p-3" id="navbar">
      <Container fluid className="appNavBar" id="navContainer">
        <Navbar.Brand as={Link} to="/">
          <img src="https://www.swirlingovercoffee.com/wp-content/uploads/2019/05/Power-Mac-Center-Logo.jpg" className="mb-4" alt='logo' width="80" height="80" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={handleToggle} />
        <Navbar.Collapse id="basic-navbar-nav" className={isCollapsed ? 'show' : ''}>
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/product">
              Products
            </Nav.Link>
            {user.id !== null && user.isAdmin === true ? (
              <NavDropdown title="Menu" id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/addProduct">
                  Add Product
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/update-user-admin">
                  Admin
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/allOrders">
                  Total orders
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout">
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : user.id !== null ? (
              <NavDropdown title="Menu" id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/profile">
                  Profile
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/userOrders">
                  Orders
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout">
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

