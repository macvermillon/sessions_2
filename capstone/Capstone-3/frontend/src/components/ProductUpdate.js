import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

function ProductUpdate({ product }) {
  // state for course id which will be used for fetching
  const [productId, setProductId] = useState('');
  // state for editcourse modal
  const [showEdit, setShowEdit] = useState(false);

  // useState for our form (modal)
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [supplierPrice, setSupplierPrice] = useState('');
  const [marginPrice, setMarginPrice] = useState('');
  const [quantity, setQuantity] = useState('');

  // function for opening the edit modal

  const openEdit = (productId) => {
    setShowEdit(true);

    // to still get the actual data from the form
    fetch(`http://localhost:4000/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductId(data._id);
        setName(data.name);
        setDescription(data.description);
        setSupplierPrice(data.supplier.map((details) => details.supplierPrice));
        setMarginPrice(data.marginPrice);
        setQuantity(data.quantity);
      });
  };

  const closeEdit = () => {
    setShowEdit(false);
    setName('');
    setDescription('');
    setSupplierPrice(0);
  };

  // function to save our update
  const editProduct = (e, productId) => {
    fetch(`http://localhost:4000/products/${productId}/edit`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        supplierPrice: supplierPrice,
        marginPrice: marginPrice,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.state === true) {
          Swal.fire({
            title: 'Update Success!',
            icon: 'success',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#f8981e',
          });

          closeEdit();
        } else {
          Swal.fire({
            title: 'Update Error!',
            icon: 'error',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
          closeEdit();
        }
      });
  };

  return (
    <>
      <Button
        variant="primary"
        className="circle-btn btn-flask"
        size="sm"
        onClick={() => {
          openEdit(product);
        }}
      >
        Update
      </Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e, productId)}>
          <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" required value={name} onChange={(e) => setName(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                required
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="supplierPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                required
                value={supplierPrice}
                onChange={(e) => setSupplierPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="marginPrice">
              <Form.Label>Margin Price</Form.Label>
              <Form.Control
                type="number"
                required
                value={marginPrice}
                onChange={(e) => setMarginPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="marginPrice">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                required
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" className="close-btn" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" className="login-btn" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
export default ProductUpdate;
