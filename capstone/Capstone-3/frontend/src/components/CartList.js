import { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';

import CheckOut from '../components/CheckOut';
import SingleCheckOut from '../components/SingleCheckOut';

function CartList() {
  const [cart, setCart] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [error, setError] = useState(false);

  useEffect(() => {
    fetch(`http://localhost:4000/cart/viewCart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.state === false) {
          setError(true);
        } else {
          setCart(result.cart);
          setTotalPrice(result.price);
        }
      })
      .catch((error) => console.log(error));
  }, []);
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 10;
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const records = cart.slice(firstIndex, lastIndex);
  const npage = Math.ceil(cart.length / recordsPerPage);
  const numbers = [...Array(npage + 1).keys()].slice(1);

  const prePage = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const changeCPage = (id) => {
    setCurrentPage(id);
  };

  const nextPage = () => {
    if (currentPage === lastIndex) {
    } else {
      setCurrentPage(currentPage + 1);
    }
  };
  return (
    <>
      {error === false ? (
        records.map((crt, i) => (
          <Col key={i} xs="12" lg={{ offset: 2, span: 8 }} className="mb-4">
            <Card className="cardHighlight">
              {/* <Card.Img variant="top" src={require('../images/logos/flaskhub.png')} /> */}
              <Card.Body>
                <Card.Title>
                  <strong>{crt.name}</strong>
                </Card.Title>
                <Card.Text>{crt.description}</Card.Text>
                <Card.Text>
                  <h5>
                    <strong>Php {crt.price}</strong>
                  </h5>
                </Card.Text>
                <Card.Text>
                  <h6>
                    <strong>Quantity: </strong>
                    {crt.quantity}
                  </h6>
                </Card.Text>
                <Card.Text>
                  <h6>Subtotal: {crt.quantity * crt.price}</h6>
                </Card.Text>
                <Card.Text>
                  <SingleCheckOut productId={crt.productId} />
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))
      ) : (
        <h1 className="text-center">No Products Found</h1>
      )}
      <nav>
        <ul className="pagination">
          <li className="page-item">
            <a href="#/" className="page-link" onClick={prePage}>
              Prev
            </a>
          </li>

          {numbers.map((n, i) => (
            <li className={`page-item ${currentPage === n ? 'active' : ''}`} key={i}>
              <a href="#/" className="page-link" onClick={() => changeCPage(n)}>
                {n}
              </a>
            </li>
          ))}
          <li className="page-item">
            <a href="#/" className="page-link" onClick={() => nextPage()}>
              Next
            </a>
          </li>
        </ul>
      </nav>
      <footer>
        <>
          <Row className="px-5">
            <Col xs="12" md="12" lg="12">
              <h2 className="float-end">Total Price: {totalPrice}</h2>
            </Col>
            <Col xs="12" md="12" lg="12">
              <CheckOut />
            </Col>
          </Row>
        </>
      </footer>
    </>
  );
}

export default CartList;
