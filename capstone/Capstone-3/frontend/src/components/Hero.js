import { Carousel } from 'react-bootstrap';

function Hero() {
  return (
    <Carousel fade>
      <Carousel.Item>
        <img
          src={require('../images/contents/hero_1.jpg')}
          alt="flaskhub.png"
          className="img-fluid"
          width="100%"
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          src={require('../images/contents/hero_2.jpg')}
          alt="flaskhub.png"
          className="img-fluid"
          width="100%"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          src={require('../images/contents/hero_3.jpg')}
          alt="flaskhub.png"
          className="img-fluid"
          width="100%"
        />
      </Carousel.Item>
    </Carousel>
  );
}

export default Hero;
