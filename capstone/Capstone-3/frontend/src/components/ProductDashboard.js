import { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';

import ProductFeature from './ProductFeature';
import ProductState from './ProductState';
import ProductUpdate from './ProductUpdate';

function ProductDashboard() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/all`)
      .then((response) => response.json())
      .then((result) => setProducts(result))
      .catch((error) => console.log(error));
  }, []);
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 5;
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const records = products.slice(firstIndex, lastIndex);
  const npage = Math.ceil(products.length / recordsPerPage);
  const numbers = [...Array(npage + 1).keys()].slice(1);

  const prePage = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const changeCPage = (id) => {
    setCurrentPage(id);
  };

  const nextPage = () => {
    if (currentPage === lastIndex) {
    } else {
      setCurrentPage(currentPage + 1);
    }
  };
  console.log(lastIndex);
  return (
    <Row>
      <Col className="col-12 px-5">
        <h1 className="text-center my-4"> Admin Dashboard</h1>
        <div>
          <table className="table table-bordered table-striped table-responsive">
            <thead className="text-center">
              <tr>
                <th>ID</th>
                <th>SKU</th>
                <th>Name</th>
                <th>Description</th>
                <th>Margin</th>
                <th>Supplier Price</th>
                <th>Sell Price</th>
                <th>Quantity</th>
                <th>Availability</th>
                <th>State</th>
                <th colSpan="3">Actions</th>
              </tr>
            </thead>
            <tbody>
              {products && products.length > 0 ? (
                records.map((prd, i) => (
                  <tr key={i}>
                    <td>{prd._id}</td>
                    <td>{prd.sku}</td>
                    <td>{prd.name}</td>
                    <td>{prd.description}</td>
                    <td>{prd.marginPrice * 100}%</td>
                    <td>{prd.supplier.map((details) => details.supplierPrice)}</td>
                    <td>{prd.price}</td>
                    <td>{prd.quantity}</td>
                    <td className={prd.isActive ? 'text-success' : 'text-danger'}>
                      {prd.isActive ? 'Available' : 'Unavailable'}
                    </td>
                    <td className={prd.featured ? 'text-info' : 'text-'}>
                      {prd.featured ? 'Featured' : 'Unfeatured'}
                    </td>
                    <td>
                      <ProductUpdate product={prd._id} fetchData={products} />
                    </td>
                    <td>
                      <ProductFeature product={prd._id} fetchData={products} featured={prd.featured} />
                    </td>
                    <td>
                      <ProductState product={prd._id} isActive={prd.isActive} fetchData={products} />
                    </td>
                  </tr>
                ))
              ) : (
                <h1 className="text-center">No Products Found</h1>
              )}
            </tbody>
          </table>
          <footer>
            <ul className="pagination">
              <li className="page-item">
                <a href="#/" className="page-link" onClick={prePage}>
                  Prev
                </a>
              </li>

              {numbers.map((n, i) => (
                <li className={`page-item ${currentPage === n ? 'active' : ''}`} key={i}>
                  <a href="#/" className="page-link" onClick={() => changeCPage(n)}>
                    {n}
                  </a>
                </li>
              ))}
              <li className="page-item">
                <a href="#/" className="page-link" onClick={() => nextPage()}>
                  Next
                </a>
              </li>
            </ul>
          </footer>
        </div>
      </Col>
    </Row>
  );
}

export default ProductDashboard;
