import React, { useState } from 'react';
import ProductCard from './ProductCard';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(`http://localhost:4000/product/search`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
      console.log(data);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  return (
    <div className='form pt-5 container'>
      <div className="form-group" id="searchInput">
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
          placeholder="Search Product Name"
          required="required"
        />
        <i></i>

      </div>
      <button id='productButton' className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <ul>
        {searchResults.map(product => (
          <ProductCard productProp={product} key={product._id} />
        ))}
      </ul>
    </div>
  );
};

export default ProductSearch;

