import { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function ProductList() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/all`)
      .then((response) => response.json())
      .then((result) => setProducts(result))
      .catch((error) => console.log(error));
  }, []);
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 20;
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const records = products.slice(firstIndex, lastIndex);
  const npage = Math.ceil(products.length / recordsPerPage);
  const numbers = [...Array(npage + 1).keys()].slice(1);

  const prePage = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const changeCPage = (id) => {
    setCurrentPage(id);
  };

  const nextPage = () => {
    if (currentPage === lastIndex) {
    } else {
      setCurrentPage(currentPage + 1);
    }
  };
  return (
    <>
      {products && products.length > 0 ? (
        records.map((prd, i) => (
          <Col key={i} xs="12" lg="3" className="mb-4">
            <Card className="cardHighlight">
              {/* <Card.Img variant="top" src={require('../images/logos/flaskhub.png')} /> */}
              <Card.Body>
                <Card.Text className="text-center">
                  <img src={prd.image} height="200" alt="img"></img>
                </Card.Text>
                <Card.Title>
                  <strong>{prd.name}</strong>
                </Card.Title>
                <Card.Text>{prd.description}</Card.Text>
                <Row>
                  <Col xs="12" lg="6">
                    <h5>
                      <strong>Php {prd.price}</strong>
                    </h5>
                  </Col>
                  <Col xs="12" lg="6">
                    <Link variant="primary" className="text-link login-btn" to={`/products/${prd._id}`}>
                      View Product
                    </Link>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Col>
        ))
      ) : (
        <h1 className="text-center">No Products Found</h1>
      )}
      <footer>
        <ul className="pagination">
          <li className="page-item">
            <a href="#/" className="page-link" onClick={prePage}>
              Prev
            </a>
          </li>

          {numbers.map((n, i) => (
            <li className={`page-item ${currentPage === n ? 'active' : ''}`} key={i}>
              <a href="#/" className="page-link" onClick={() => changeCPage(n)}>
                {n}
              </a>
            </li>
          ))}
          <li className="page-item">
            <a href="#/" className="page-link" onClick={() => nextPage()}>
              Next
            </a>
          </li>
        </ul>
      </footer>
    </>
  );
}

export default ProductList;
