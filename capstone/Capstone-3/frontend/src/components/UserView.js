import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';

export default function UserView({productData}) {

	const [product, setProduct] = useState([])

	useEffect(() => {
		const productArr = productData.map(product => {

			//only render the active courses since the route used is /all from Course.js page
			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>
					)
			} else {
				return null;
			}
		})

		setProduct(productArr)
		
	}, [productData])

	return(
		<>	
			{ product }
		</>
	)
}