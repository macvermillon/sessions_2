import React from 'react';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AddProduct from './pages/AddProduct';
import Error from './pages/Error';
import Product from './pages/Product';
import ProductView from './pages/ProductView';
import Profile from './pages/Profile';
import Orders from './pages/Orders';
import TotalOrders from './pages/TotalOrders';
import UpdateUserAsAdmin from './pages/UpdateUserAsAdmin';
import './App.css';
import { UserProvider } from './context/UserContext';
import AppNavbar from './components/AppNavBar';

export default function App() {

  // This state hook is for the user state that's defined here for the global.
  const [user, setUser] = useState({
        id: null,
        isAdmin: null
  });

  // Function for clearing the localStorage to log out the user.
  const unsetUser = () => {
    localStorage.clear();
  }

  // used to check if the user information is properly stored upon login and the localStorage information is cleared out upon logout.
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  
  return (

        <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
            <Container fluid className="appContainer">
              <AppNavbar/>
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/product" element={<Product/>}/>
                  <Route path="/userOrders" element={<Orders/>}/>
                  <Route path="/allOrders" element={<TotalOrders/>}/>
                  <Route path="/update-user-admin" element={<UpdateUserAsAdmin/>}/>
                  <Route path="/product/:productId" element={<ProductView/>}/>
                  <Route path="/addProduct" element={<AddProduct/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="*" element={<Error/>}/>
                  <Route path="/profile" element={<Profile/>}/>
              </Routes>
            </Container>
          </Router>
        </UserProvider>
    )
};


