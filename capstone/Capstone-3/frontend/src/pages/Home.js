import Hightlights from '../components/Highlights';
import Banner from '../components/Banner';
import {Container} from 'react-bootstrap';

export default function Home() {

	const data = {
		title: "PowerMacCenter",
		content: "Your Premier Apple Partner",
		destination: "/product",
		label: "Shop Now!"
	}

	return (
		<Container id="appHome">
			<>
				<Banner data={data}/>
				<Hightlights/>
			</>

			<footer className="footer mt-5">
			
			     <p>&copy; {new Date().getFullYear()} PowerMacCenter</p>

			</footer> 
		</Container>


	)
};