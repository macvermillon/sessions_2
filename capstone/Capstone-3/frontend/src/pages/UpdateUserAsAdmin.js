import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UpdateUserAsAdmin = () => {
  const [userId, setUserId] = useState('');
  const [message, setMessage] = useState('');

  const handleUserIdChange = (event) => {
    setUserId(event.target.value);
  };

  const handleUpdateUserAsAdmin = async () => {
    try {
      // Assuming you have an authentication token for the admin user
      const adminAuthToken = localStorage.getItem('token');

      // Request body with the user ID to be updated
      const requestBody = {
        userId: userId,
      };

      // Set the Authorization header with the admin token
      const headers = new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${adminAuthToken}`,
      });

      // Make sure to replace 'YOUR_API_BASE_URL' with your actual backend API base URL
      const apiUrl = `http://localhost:4000/users/update-user-admin`;

      const refreshInput = () => {
        setUserId('');
      }

      // Send the PUT request to update the user as admin
      const response = await fetch(apiUrl, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(requestBody),
      });
      if(response) {
        Swal.fire({
            title: 'Updated Successfully',
            icon: 'success',
            text: 'User updated as admin successfully'
          });
        setUserId('');
      }

    } catch (error) {
      console.error('Error updating user as admin:', error);
      setMessage('Error updating user as admin');
    }
  };

  return (
    
    <div className="container text-center mt-5" id="user">
      <h2>Update User as Admin</h2>
      <div>
        <label className="mx-2">User ID:</label>
        <input type="text" value={userId} onChange={handleUserIdChange} id="input"></input>
      </div>
      <Button onClick={handleUpdateUserAsAdmin} className="mt-4 mb-3" id="update">Update</Button>
      <p>{message}</p>
    </div>
    
  );
};

export default UpdateUserAsAdmin;
