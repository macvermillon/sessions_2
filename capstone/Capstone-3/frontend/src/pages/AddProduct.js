import {
  faClipboardQuestion,
  faFileSignature,
  faHandHoldingDollar,
  faHashtag,
  faMagnifyingGlassDollar,
  faPenFancy,
  faPlus,
  faQrcode,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { Button, Form, InputGroup, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

function AddProduct() {
  const ava_image =
    'https://res.cloudinary.com/da3lvrezp/image/upload/v1697056922/products/upload_image_oh1pki.png';
  const [image, setImage] = useState('');
  const [sku, setSku] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [subCategory, setSubCategory] = useState('');
  const [quantity, setQuantity] = useState();
  const [supplierPrice, setSupplierPrice] = useState();
  const [marginPrice, setMarginPrice] = useState();
  const [isActive, setIsActive] = useState(false);

  const [showAdd, setShowAdd] = useState(false);
  const openAdd = (loginState) => {
    setShowAdd(loginState);
  };
  const closeEdit = () => {
    setShowAdd(false);
  };

  const handleFileUpload = async (e) => {
    const file = e.target.files[0];
    convertToBase64(file);
  };

  function convertToBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImage(reader.result);
    };
  }
  const addProduct = (e) => {
    e.preventDefault();
    fetch(`http://localhost:4000/products/add-product`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        image: image,
        sku: sku,
        name: name,
        description: description,
        subCategory: subCategory,
        marginPrice: marginPrice,
        supplierPrice: supplierPrice,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.state === true) {
          Swal.fire({
            icon: 'success',
            title: 'Congratulations!',
            text: data.message,
            imageWidth: 240,
            imageHeight: 60,
            imageAlt: 'FlaskHub',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#f8981e',
          }).then(function () {
            window.location = '/dashboard';
          });
          setSku('');
          setName('');
          setDescription('');
          setSubCategory('');
          setQuantity('');
          setMarginPrice('');
          setSupplierPrice('');
          closeEdit();
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Uh Oh! Something went wrong!',
            text: data.message,
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };

  useEffect(() => {
    if (
      sku !== '' &&
      name !== '' &&
      description !== '' &&
      subCategory !== '' &&
      quantity !== 0 &&
      supplierPrice !== 0 &&
      marginPrice !== 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [sku, name, description, subCategory, quantity, supplierPrice, marginPrice]);
  return (
    <>
      <Button className="add-button" title="Add Product" onClick={() => openAdd(true)}>
        <FontAwesomeIcon icon={faPlus} />
      </Button>
      <Modal
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={showAdd}
        onHide={closeEdit}
      >
        <Form onSubmit={(e) => addProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>  
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 text-center">
              <h5>Product Image</h5>
              <Form.Label htmlFor="file-upload" className="custom-file-upload" >
                <img src={image !== '' ? image : ava_image} alt="" width="100" height="100" />
              </Form.Label>

              <input
                type="file"
                lable="Image"
                name="myFile"
                id="file-upload"
                accept=".jpeg, .png, .jpg"
                onChange={(e) => handleFileUpload(e)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faQrcode} />
                </InputGroup.Text>
                <Form.Control
                  type="text"
                  placeholder="enter product sku"
                  value={sku}
                  onChange={(e) => {
                    setSku(e.target.value);
                  }}
                  required
                />
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faFileSignature} />
                </InputGroup.Text>
                <Form.Control
                  type="text"
                  placeholder="enter product name"
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                  required
                />
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faPenFancy} />
                </InputGroup.Text>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="enter product description"
                  value={description}
                  onChange={(e) => {
                    setDescription(e.target.value);
                  }}
                />
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faClipboardQuestion} />
                </InputGroup.Text>
                <Form.Select
                  type="text"
                  placeholder="enter product quantity"
                  value={subCategory}
                  onChange={(e) => {
                    setSubCategory(e.target.value);
                  }}
                  required
                >
                  <option>-- subcategory --</option>
                  <option value="MAC">MAC</option>
                  <option value="iPhone">iPhone</option>
                  <option value="iPAD">iPAD</option>
                  <option value="iWatch">iWatch</option>
                  <option value="Accessories">Accessories</option>
                </Form.Select>
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faHashtag} />
                </InputGroup.Text>
                <Form.Control
                  type="number"
                  min="0"
                  placeholder="enter  product quantity"
                  value={quantity}
                  onChange={(e) => {
                    setQuantity(e.target.value);
                  }}
                  required
                />
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faHandHoldingDollar} />
                </InputGroup.Text>
                <Form.Control
                  type="number"
                  placeholder="enter margin price"
                  value={marginPrice}
                  onChange={(e) => {
                    setMarginPrice(e.target.value);
                  }}
                  required
                />
              </InputGroup>
            </Form.Group>
            <Form.Group className="mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faMagnifyingGlassDollar} />
                </InputGroup.Text>
                <Form.Control
                  type="number"
                  placeholder="enter supplier price"
                  value={supplierPrice}
                  onChange={(e) => {
                    setSupplierPrice(e.target.value);
                  }}
                  required
                />
              </InputGroup>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button className="close-btn" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="primary" className="login-btn" type="submit" disabled={isActive === false}>
              ADD PRODUCT
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
export default AddProduct;
