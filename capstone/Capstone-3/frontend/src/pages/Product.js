import { Row } from 'react-bootstrap';
// import ProductData from '../../components/ProductData';
import ProductList from '../components/ProductList';
function Products() {
  return (
    <>
      <Row className="m-3">
        <h2 className="text-center">
          <strong>ALL PRODUCTS</strong>
        </h2>
        <hr className="text-center" style={{ width: '50%' }} />
      </Row>
      <Row className="m-3">
        {/* <ProductData /> */}

        <ProductList />
      </Row>
    </>
  );
}

export default Products;
