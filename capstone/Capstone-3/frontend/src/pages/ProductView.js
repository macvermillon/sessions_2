import { useContext, useEffect, useState } from 'react';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../context/UserContext';

import Login from './Login';

function ProductView() {
  const { user } = useContext(UserContext);
  const { id } = useParams();
  const navigate = useNavigate();

  const [image, setImage] = useState('');
  const [sku, setSku] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [isActive, setIsActive] = useState(false);

  const addToCart = (productId) => {
    fetch(`http://localhost:4000/cart/addToCart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.state === true) {
          Swal.fire({
            title: 'Added to cart successfully!',
            text: 'You have successfully added this to cart',
            icon: 'success',
            confirmButtonText: 'Ok',
          });
          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong!',
            text: 'Please try again!',
            icon: 'error',
            confirmButtonText: 'Ok',
          });
        }
      });
  };
  useEffect(() => {
    fetch(`http://localhost:4000/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setImage(data.image.secure_url);
        setSku(data.sku);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.quantity);
      });
  }, [id]);

  const increaseVal = () => {
    if (quantity >= 0 && quantity < stock) {
      setQuantity(quantity + 1);
    } else {
      setQuantity(quantity);
    }
  };

  const decreaseVal = () => {
    if (quantity > 0 && quantity <= stock) {
      setQuantity(quantity - 1);
    } else {
      setQuantity(quantity);
    }
  };

  useEffect(() => {
    if (quantity > 0 && quantity <= stock) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [quantity, stock]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="">
              <Card.Text className="text-center">
                <img src={image} height="200" alt="img"></img>
              </Card.Text>
              <Card.Title>
                <strong>{name}</strong>
              </Card.Title>
              <Card.Subtitle>Item#: {sku}</Card.Subtitle>
              <Card.Subtitle className="mt-3">Product Description</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Text>
                <strong>PhP {price}</strong>
              </Card.Text>
              <Card.Text>
                <strong>Stock: </strong>
                {stock} pc/s
              </Card.Text>
              <Card.Text>
                <Button className="change-btn text-center" onClick={decreaseVal}>
                  -
                </Button>
                <input
                  type="number"
                  className="quantity-input input-number"
                  value={quantity}
                  onChange={(e) => setQuantity(e.target.value)}
                />
                <Button className="change-btn text-center" onClick={increaseVal}>
                  +
                </Button>
              </Card.Text>
              {user.id !== null ? (
                <Button
                  variant="primary"
                  className="login-btn"
                  onClick={() => addToCart(id)}
                  disabled={isActive === false}
                >
                  Add To Cart
                </Button>
              ) : (
                <Login showLogin={true} />
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default ProductView;
