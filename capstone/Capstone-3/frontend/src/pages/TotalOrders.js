import { useState, useEffect } from 'react';
import { Container, Table } from 'react-bootstrap';

export default function Order() {
  const [orders, setOrders] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    // Fetch all orders for all users (admin only)
    fetch(`http://localhost:4000/order/allOrders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setOrders(data.orders);
        // Calculate the total amount from all orders
        const total = data.orders.reduce((acc, order) => acc + order.totalAmount, 0);
        setTotalAmount(total);
      })
      .catch(error => console.error(error));
  }, []);

  return (
    <Container className="mt-5" id="totalOrder">
      <h2 className="text-center p-3">All User Orders</h2>
      {orders.length > 0 ? (
        <Table striped bordered responsive>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>User ID</th>
              <th>Product ID</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Purchased On</th>
              <th>Total Amount</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              <tr key={order._id}>
                <td>{order._id}</td>
                <td>{order.userId}</td>
                <td>{order.products[0].productId}</td>
                <td>{order.products[0].quantity}</td>
                <td>{order.products[0].price}</td>
                <td>{order.purchasedOn}</td>
                <td>{order.totalAmount}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="6" className="text-end fw-bold">Total Amount of all Orders:</td>
              <td>{totalAmount}</td>
            </tr>
          </tfoot>
        </Table>
      ) : (
        <p>No orders found.</p>
      )}
    </Container>
  );
}
