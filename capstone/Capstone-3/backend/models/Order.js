// Dependencies
const mongoose = require('mongoose');

// Order Schema
const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'Please add a userId'],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, 'Please add a productId'],
      },
      name: {
        type: String,
        required: [true, 'Please add a name'],
      },
      quantity: {
        type: Number,
        required: [true, 'Please add a quantity'],
      },
      subTotal: {
        type: Number,
        required: [true, 'Please add a subTotal'],
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, 'Please add a totalAmount'],
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
  status: {
    type: String,
    default: 'Pending',
  },
});

module.exports = mongoose.model('Order', orderSchema);
