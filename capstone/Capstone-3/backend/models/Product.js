// Dependencies
const mongoose = require('mongoose');

// Product Schema
const productSchema = new mongoose.Schema({
  image: {
    public_id: {
      type: String,
      required: true,
    },
    secure_url: {
      type: String,
      required: true,
    },
    url: {
      type: String,
      required: true,
    },
  },
  sku: {
    type: String,
    required: [true, 'Please add a product code'],
  },
  name: {
    type: String,
    required: [true, 'Please add a product name'],
  },
  description: {
    type: String,
    required: [true, 'Please add a product description'],
  },
  supplier: [
    {
      supplierId: {
        type: String,
      },
      supplierName: {
        type: String,
      },
      supplierPrice: {
        type: Number,
      },
    },
  ],
  category: {
    type: String,
    default: 'Drinkware',
  },
  subCategory: {
    type: String,
    required: [true, 'Please add a product sub category'],
  },
  marginPrice: {
    type: Number,
    required: [true, 'Please add a product mark up'],
  },
  price: {
    type: Number,
    required: [true, 'Please add a product price'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  quantity: {
    type: Number,
    required: [true, 'Please add a product quantity'],
  },
  featured: {
    type: Boolean,
    default: false,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model('Products', productSchema);
