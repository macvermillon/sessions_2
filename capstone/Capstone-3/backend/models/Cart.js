// Dependencies
const mongoose = require('mongoose');

// Orders Schema
const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'User ID is required'],
  },
  productId: {
    type: String,
    required: [true, 'Product ID is required'],
  },
  name: {
    type: String,
    required: [true, 'Product Name is required'],
  },
  description: {
    type: String,
    required: [true, 'Description is required'],
  },
  quantity: {
    type: Number,
    required: [true, 'Quantity is required'],
  },
  price: {
    type: Number,
    required: [true, 'Price is required'],
  },
  subTotal: {
    type: Number,
    required: [true, 'Total Price is required'],
  },
  cartedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model('Cart', cartSchema);
