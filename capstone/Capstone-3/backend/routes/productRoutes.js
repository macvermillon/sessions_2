// Dependencies
const express = require('express');

// Routing Component
const router = express.Router();

// Controllers
const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// Destructure Auth
const { verify, verifyAdmin } = auth;

// Add Product
router.post('/add-product', verify, verifyAdmin, productController.addProduct);

// Get All Products
router.get('/all', productController.getAllProducts);

// Get Active Products
router.get('/active', productController.getActiveProducts);

// Get Archived Products
router.get('/archive', verify, verifyAdmin, productController.getArchivedProducts);

// Get Specific Product [POST]
router.post('/search', productController.searchProduct);

// Get Specific Product [GET]
router.get('/:id', productController.getProduct);

// Update Product
router.put('/:id/edit', verify, verifyAdmin, productController.updateProduct);

// Archive Product
router.put('/:id/archive', verify, verifyAdmin, productController.archiveProduct);

// Activate Product
router.put('/:id/activate', verify, verifyAdmin, productController.activateProduct);

// Unfeature Product
router.put('/:id/unfeature', verify, verifyAdmin, productController.unFeatureProduct);

// Feature Product
router.put('/:id/feature', verify, verifyAdmin, productController.featureProduct);

// View Products by Category
router.get('/category/:category', productController.viewProductsByCategory);

// View Products by Subcategory
router.get('/:category/:subcategory', productController.viewProductsBySub);

module.exports = router;
