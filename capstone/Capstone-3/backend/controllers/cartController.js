// Dependencies
const Cart = require('../models/Cart.js');
const Products = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Add To Cart
const addToCart = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    const productDetails = await Products.findById(req.body.productId).then((product) => product);
    let quantity = req.body.quantity ? req.body.quantity : 1;
    if (quantity > productDetails.quantity) {
      quantity = productDetails.quantity;
    }
    const subTotal = productDetails.price * quantity;
    const newCart = await new Cart({
      userId: req.user.id,
      productId: productDetails._id,
      name: productDetails.name,
      description: productDetails.description,
      quantity: quantity,
      price: productDetails.price,
      subTotal: subTotal,
    });
    await Cart.findOne({ userId: req.user.id, productId: productDetails._id }).then((status) => {
      if (status && productDetails.isActive === true && productDetails.quantity > 0) {
        let addQuantity = status.quantity + quantity;
        if (addQuantity > productDetails.quantity) {
          addQuantity = productDetails.quantity;
        }
        let newTotal = addQuantity * status.price;
        Cart.findByIdAndUpdate(status._id, { quantity: addQuantity, subTotal: newTotal }).then(
          async (update, err) => {
            if (err) {
              res.send({ state: false, message: 'Something went wrong! Please try again!' });
            } else {
              res.send({ state: true, message: 'Item Carted Successfully' });
            }
          }
        );
      } else if (!status && productDetails.isActive === true && productDetails.quantity > 0) {
        return newCart.save().then(async (cart, err) => {
          if (err) {
            res.send({ state: false, message: 'Internal Server Error' });
          } else {
            res.send({ state: true, message: 'Item Carted Successfully' });
          }
        });
      } else {
        res.send({ state: false, message: 'Item Not Found or Out of Stock' });
      }
    });
  } catch (err) {
    if (err.kind) {
      res.status(404).send('Item Not Found');
    } else {
      res.status(500).send('Internal Server Error');
    }
  }
};

// View Cart
const viewCart = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    const cart = await Cart.find({ userId: req.user.id }, { status: 0 }).then((cart) => {
      const totalAmount = cart.reduce((acc, product) => acc + product.subTotal, 0);
      if (cart.length === null || cart.length === 0) {
        return res.status(404).json({ state: false, message: 'Your Cart is Empty' });
      } else {
        res.status(200).json({ price: totalAmount, cart: cart });
      }
    });
  } catch (err) {
    res.send(err);
  }
};

// Deduct Qantity fom Cart
const deductQuantity = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    const cart = await Cart.find({ userId: req.user.id, productId: req.params.id });
    const product = await Products.findById(req.params.id);
    const cartId = cart[cart.length - 1]._id;
    if (cart.length === null || cart.length === 0) {
      return res.send({ state: false, message: 'Your Cart is Empty' });
    } else {
      if (req.body.quantity > product.quantity) {
        return res.send({ state: false, message: `Stock not sufficient at ${product.quantity}` });
      } else {
      }
      if (cart.length === null || cart.length === 0) {
        return res.status(404).send('Your Cart is Empty');
      } else {
        const quantity = req.body.quantity;
        const subTotal = product.price * quantity;
        if (quantity <= 0) {
          return res.status(404).send('Invalid Quantity! Must be more than zero');
        } else {
          Cart.findOneAndUpdate(
            { _id: cartId, userId: req.user.id, productId: req.params.id },
            { quantity: quantity, subTotal: subTotal }
          ).then((update, err) => {
            if (err) {
              res.status(500).send('Internal Server Error');
            } else {
              res.status(200).send('Quantity Updated Successfully');
            }
          });
        }
      }
    }
  } catch (err) {
    res.status(500).send(err.message);
  }
};

// Delete All Items on Cart
const deleteAll = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    return await Cart.deleteMany({ userId: req.user.id }).then((status) => {
      if (status) {
        res.status(200).send('Your Cart is now Empty!');
      } else {
        res.status(500).send('Internal Server Error');
      }
    });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

// Delete One Item
const deleteOne = async (req, res) => {
  try {
    Cart.find({ userId: req.user.id, productId: req.params.id }).then((cart) => {
      if (cart.length === 0) {
        res.status(404).send('Item Not Found');
      } else {
        Cart.deleteOne({ userId: req.user.id, productId: req.params.id }).then((status, err) => {
          if (status) {
            res.status(200).send('Item Deleted Successfully');
          } else {
            res.status(500).send('Internal Server Error: ' + err);
          }
        });
      }
    });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

// Exports
module.exports = {
  addToCart: addToCart !== undefined ? addToCart : null,
  viewCart: viewCart !== undefined ? viewCart : null,
  deductQuantity: deductQuantity !== undefined ? deductQuantity : null,
  deleteAll: deleteAll !== undefined ? deleteAll : null,
  deleteOne: deleteOne !== undefined ? deleteOne : null,
};
