// Dependencies
const Products = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const cloudinary = require('../utils/cloudinary');

// Add Products
const addProduct = async (req, res) => {
  try {
    const image = req.body.image;
    const result = await cloudinary.uploader.upload(image, {
      folder: 'products',
      width: 240,
      crop: 'scale',
    });
    let marginPrice;
    if (req.body.supplierPrice) {
      const supPrice = parseFloat(req.body.supplierPrice);
      const marPrice = parseFloat(req.body.marginPrice);
      const addPrice = marPrice * supPrice;
      marginPrice = supPrice + addPrice;
    } else {
      marginPrice = req.body.price;
    }

    const newProduct = await new Products({
      image: {
        public_id: result.public_id,
        secure_url: result.secure_url,
        url: result.url,
      },
      sku: req.body.sku,
      name: req.body.name,
      description: req.body.description,
      supplier: {
        supplierId: req.body.supplierId,
        supplierName: req.body.supplierName,
        supplierPrice: req.body.supplierPrice,
      },
      category: req.body.category,
      subCategory: req.body.subCategory,
      marginPrice: req.body.marginPrice,
      price: marginPrice,
      quantity: req.body.quantity,
    });

    await Products.find({ sku: req.body.sku }).then((product) => {
      if (product.length > 0) {
        return res.status(400).json({ state: false, message: 'SKU already exists' });
      } else {
        return newProduct.save().then((newProduct, err) => {
          if (err) {
            return res.status(400).json({ state: false, message: '400 Internal Server Error' });
          } else {
            res.send({
              state: true,
              message: 'Product Added Successfully!',
            });
          }
        });
      }
    });
  } catch (error) {
    res.status(500).json({ state: false, message: '500 Internal Server Error' });
  }
};

// Get All Products
const getAllProducts = (req, res) => {
  return Products.find({})
    .then((products) => {
      if (products.length > 0) {
        return res.send(products);
      } else {
        return res.send({ state: false, message: 'No products found.' });
      }
    })
    .catch((err) => res.send(err));
};

// Get Active Products
const getActiveProducts = (req, res) => {
  Products.find(
    { isActive: true, quantity: { $gte: 1 } },
    { marginPrice: 0, supplier: 0, createdOn: 0, __v: 0 }
  ).then((products) => {
    if (products.length > 0) {
      return res.status(200).json(products);
    } else {
      return res.status(400).json({ error: 'No active products found.' });
    }
  });
};

// Get Archived Products
const getArchivedProducts = (req, res) => {
  Products.find({ isActive: false }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No archived products found.' });
    }
  });
};

// Get Specific Product [POST]
const searchProduct = (req, res) => {
  Products.find({ _id: req.body.id, isActive: true }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No products found.' });
    }
  });
};

// Get Specific Product [GET]
const getProduct = (req, res) => {
  return Products.findById(req.params.id)
    .then((result) => {
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// Update Product
const updateProduct = (req, res) => {
  const supplierPrice = parseFloat(req.body.supplierPrice);
  const marginalPrice = parseFloat(req.body.marginPrice);
  const addPrice = supplierPrice * marginalPrice;
  const marginPrice = addPrice + supplierPrice;
  const updateProduct = {
    sku: req.body.sku,
    name: req.body.name,
    description: req.body.description,
    supplier: {
      supplierPrice: supplierPrice,
    },
    category: req.body.category,
    subCategory: req.body.subCategory,
    marginPrice: marginalPrice,
    price: marginPrice,
    quantity: req.body.quantity,
  };
  Products.findByIdAndUpdate(req.params.id, updateProduct).then((products, err) => {
    if (err) {
      return res.status(400).json({ state: false, message: '400 Internal Server Error' });
    } else {
      return res.status(200).json({ state: true, message: 'Product updated successfully' });
    }
  });
};

// Archive Product
const archiveProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.isActive === true) {
      Products.findByIdAndUpdate(req.params.id, { isActive: false }).then((product, err) => {
        if (err) {
          return res.status(400).json({ state: false, message: '400 Internal Server Error' });
        } else {
          return res.status(200).json({
            state: true,
            message: 'Product Archived Successfully!',
          });
        }
      });
    } else {
      return res.status(400).json({ state: false, message: 'Product is already archived' });
    }
  });
};

// Activate Product
const activateProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.isActive === false) {
      Products.findByIdAndUpdate(req.params.id, { isActive: true }).then((product, err) => {
        if (err) {
          return res.status(400).json({ state: false, message: '400 Internal Server Error' });
        } else {
          return res.status(200).json({
            state: true,
            message: 'Product successfully activated.',
          });
        }
      });
    } else {
      return res.status(400).json({ state: false, message: 'Product is already active' });
    }
  });
};

// Archive Product
const unFeatureProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.featured === true) {
      Products.findByIdAndUpdate(req.params.id, { featured: false }).then((product, err) => {
        if (err) {
          return res.status(400).json({ state: false, message: '400 Internal Server Error' });
        } else {
          return res.status(200).json({
            state: true,
            message: 'Product Unfeatured Successfully!',
          });
        }
      });
    } else {
      return res.status(400).json({ state: false, message: 'Product is already unfeatured' });
    }
  });
};

// Activate Product
const featureProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.featured === false) {
      Products.findByIdAndUpdate(req.params.id, { featured: true }).then((product, err) => {
        if (err) {
          return res.status(400).json({ state: false, message: '400 Internal Server Error' });
        } else {
          return res.status(200).json({
            state: true,
            message: 'Product successfully featured.',
          });
        }
      });
    } else {
      return res.status(400).json({ state: false, message: 'Product is already featured' });
    }
  });
};

// View Products by Category
const viewProductsByCategory = (req, res) => {
  Products.find({ category: new RegExp(`^${req.params.category}$`, 'i'), isActive: true }).then(
    (products) => {
      if (products.length > 0) {
        return res.status(200).json({ products: products });
      } else {
        return res.status(400).json({ error: 'No products found.' });
      }
    }
  );
};

// View Products by Subcategory
const viewProductsBySub = (req, res) => {
  Products.find({
    category: new RegExp(`^${req.params.category}$`, 'i'),
    subCategory: new RegExp(`^${req.params.subcategory}$`, 'i'),
    isActive: true,
  }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No products found.' });
    }
  });
};

module.exports = {
  addProduct: addProduct !== undefined ? addProduct : null,
  getAllProducts: getAllProducts !== undefined ? getAllProducts : null,
  getActiveProducts: getActiveProducts !== undefined ? getActiveProducts : null,
  getArchivedProducts: getArchivedProducts !== undefined ? getArchivedProducts : null,
  searchProduct: searchProduct !== undefined ? searchProduct : null,
  getProduct: getProduct !== undefined ? getProduct : null,
  updateProduct: updateProduct !== undefined ? updateProduct : null,
  archiveProduct: archiveProduct !== undefined ? archiveProduct : null,
  activateProduct: activateProduct !== undefined ? activateProduct : null,
  unFeatureProduct: unFeatureProduct !== undefined ? unFeatureProduct : null,
  featureProduct: featureProduct !== undefined ? featureProduct : null,
  viewProductsByCategory: viewProductsByCategory !== undefined ? viewProductsByCategory : null,
  viewProductsBySub: viewProductsBySub !== undefined ? viewProductsBySub : null,
};
