import { useContext } from 'react';
import { Navigate } from 'react-router-dom';
// Context Component
import userContext from '../context/UserContext';

import ProfileCard from '../components/ProfileCard';

export default function Profile() {
  // Context Hooks
  const { user } = useContext(userContext);
  // const showData = fetch('http://localhost:4000/users/').then(function (response) {
  //   return response.json();
  // });
  // console.log(showData);
  return user.id !== null ? (
    <>
      <ProfileCard></ProfileCard>
    </>
  ) : (
    <Navigate to="/login" />
  );
}
