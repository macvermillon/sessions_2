import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
export default function Home() {
  const bannerText = {
    title: 'Zuitt Coding Bootcamp',
    message: 'Opportunities for everyone, everywhere',
    text: 'Enroll Now!',
  };
  return (
    <>
      <Banner key={bannerText.title} bannerProp={bannerText} />;
      <Highlights />
    </>
  );
}
