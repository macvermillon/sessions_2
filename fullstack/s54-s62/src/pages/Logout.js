import { useContext, useEffect } from "react";
import { Navigate } from 'react-router-dom';

// Context Component
import UserContext from "../context/UserContext";

export default function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    unsetUser();
    useEffect(() => {
        setUser({
            id: null,
            isAdmin: null
        });
    });

    // localStorage.clear();
    // Redirect back to login
    return <Navigate to='/login' />   
}