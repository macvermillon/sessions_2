import Banner from '../components/Banner';

export default function NotFound() {
  const bannerText = {
    id: 1,
    title: '404 - Not Found',
    message: 'The Page you are looking for cannot be found',
    text: 'Back Home',
    link: '/',
  };
  return (
    <>
      <Banner key={bannerText.id} bannerProp={bannerText} />
    </>
  );
}

