import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({ course }) {
  const archiveCourse = (courseId) => {
    fetch(`http://localhost:4000/courses/${courseId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Course Archived!',
            text: 'Course Archiving Successfully',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#0275d8',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Course Archiving Failed!',
            text: 'Please try again',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };
  return (
    <>
      <Button variant="danger" onClick={() => archiveCourse(course)}>
        Archive
      </Button>
    </>
  );
}
