import { Col, Container, Row } from 'react-bootstrap';
// import coursesData from '../data/coursesData';

export default function ProfileCard() {
  return (
    <Container className="bg-primary text-white">
      <Row className="p-5">
        <Col>
          <h1>Profile</h1>
          <h3>James Dela Cruz</h3>
          <hr></hr>
          <h4>Contacts</h4>
          <ul>
            <li>Email: jamesDC@mail.com</li>
            <li>Mobile No.: 09211231234</li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
}
