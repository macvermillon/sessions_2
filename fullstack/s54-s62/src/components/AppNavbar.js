import React, { useContext } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

// Context Component
import UserContext from '../context/UserContext';

// Navbar Function
export default function AppNavBar() {
  const { user } = useContext(UserContext);
  // const [user, setUser] = useState(localStorage.getItem('token'));
  // console.log(user);

  return (
    <Navbar expand="lg" bg="light">
      <Container>
        <Navbar.Brand as={NavLink} to="/" exact>
          Zuitt Booking
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {/* exact - useful for NavLinks to respond with exact path not partial match */}
            <Nav.Link as={NavLink} to="/" exact>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/courses" exact>
              Courses
            </Nav.Link>
            {user.id !== null ? (
              <React.Fragment>
                <Nav.Link as={NavLink} to="/profile" exact>
                  Profile
                </Nav.Link>
                <Nav.Link as={NavLink} to="/logout" exact>
                  Logout
                </Nav.Link>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Nav.Link as={NavLink} to="/login" exact>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" exact>
                  Register
                </Nav.Link>
              </React.Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
